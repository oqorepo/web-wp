<?php 
header("Content-type: text/css; charset: UTF-8");
echo cs_get_option( 'custom-css' );


$mobile = cs_get_option('mobile_menu');

$min_mobile = isset($mobile) && $mobile ? '1025px' : '992px';
$max_mobile = isset($mobile) && $mobile ? '1024px' : '991px'; ?>


.header_top_bg {
    position: relative;
    z-index: auto;
    background-color: #ffffff;
}


.header_top_bg.fixed-header {
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 100;
}

header {
    position: relative;
    width: 100%;
    z-index: 999;
    text-align: center;
}

header.absolute {
    position: absolute;
    margin-bottom: 0;
}

header a.logo {
    text-decoration: none;
    display: block;
}

header.zindex,
footer.zindex {
    z-index: 1 !important;
}

.header_top_bg.enable_fixed.fixed {
    position: fixed;
    z-index: 1000;
    width: 100%;
    top: 0;
}

.header_trans-fixed.header_top_bg {
    background-color: transparent;
    position: fixed;
    z-index: 1000;
    top: 0;
    width: 100%;
}

.header_trans-fixed.header_top_bg.open header .logo span,
.header_trans-fixed.header_top_bg.open header .mob-nav i {
    color: #222222;
}

.single-post .header_trans-fixed.bg-fixed-color {
    margin-left: 0;
    width: 100%;
}

.top-menu {
    padding-bottom: 10px;
}

.top-menu .logo {
    display: inline-block;
}

.top-menu .logo span {
    font-family: "Muli", sans-serif;
    font-size: 28px;
    line-height: 1.2;
    font-weight: 800;
    letter-spacing: .2px;
    min-width: 170px;
    text-align: center;
    background-color: transparent;
    color: #222222;
    display: inline-block;
}

.right-menu .logo span {
    vertical-align: middle;
    text-align: left;
    font-family: "Muli", sans-serif;
    font-size: 28px;
    line-height: 1.8;
    font-weight: 800;
    letter-spacing: 0.2px;
    color: #222222;
}
.right-menu #topmenu {
    text-align: right;
}

.no-menu {
    display: inline-block;
    margin-top: 12px;
}

.header_top_bg.bg-fixed-color .top-menu .logo span,
.header_top_bg.bg-fixed-color .right-menu #topmenu ul li ul li a,
.menu_light_text .right-menu #topmenu ul li ul li a,
.socials-mob-but i,
.header_top_bg.bg-fixed-color .right-menu #topmenu ul li a,
.header_top_bg.bg-fixed-color.menu_light_text .right-menu #topmenu ul li a,
.header_top_bg.bg-fixed-color .right-menu #topmenu .search-icon-wrapper i,
.header_top_bg.bg-fixed-color.menu_light_text .right-menu #topmenu .search-icon-wrapper i,
.header_top_bg.bg-fixed-color .right-menu #topmenu .awa-shop-icon::before,
.header_top_bg.bg-fixed-color.menu_light_text .right-menu #topmenu .awa-shop-icon::before {
    color: #222222;
}


#topmenu {
    width: 100%;
    text-align: center;
    background: #ffffff;
}

#topmenu ul {
    list-style: none;
    margin: 0;
    padding: 0;
    display: inline-block;
}

#topmenu ul li {
    display: inline-block;
    position: relative;
}

#topmenu ul li a {
    font-size: 15px;
    font-family: "Muli", sans-serif;
    color: #222222;
    font-weight: 800;
    display: block;
    text-align: left;
    text-decoration: none;
    padding: 0 20px 5px;
    line-height: 45px;
    letter-spacing: 0.2px;
    transition: all .3s ease;
    -webkit-font-smoothing: antialiased;
}

.header_trans-fixed.header_top_bg.open #topmenu ul li a {
    color: #222222;
}

.top-menu #topmenu > ul > li > a,
.top-menu #topmenu ul.social > li > a {
    padding: 0;
}

#topmenu .social .fa {
    font-size: 18px;
}

.top-menu .logo img {
    max-height: 100px;
}

#topmenu ul ul {
    position: absolute;
    z-index: 999;
    left: 0;
    top: 50px;
    min-width: 250px;
    display: none;
    box-sizing: border-box;
}

.right-menu.left #topmenu ul ul {
    display: block;
}

#topmenu ul ul li::before {
    content: '';
    display: table;
    clear: both;
}

#topmenu ul ul li a {
    font-size: 13px;
    line-height: 26px;
    padding: 3px 30px;
    font-family: "Muli", sans-serif;
    letter-spacing: 0.2px;
    display: block;
    width: 100%;
    position: relative;
    -webkit-font-smoothing: antialiased;
}

#topmenu > ul > li > ul > li:hover ul {
    display: block;
}

#topmenu > ul > li > ul > li > ul {
    left: 101%;
    top: -15px;
}

.mob-nav {
    display: none;
    width: 20px;
    height: 20px;
    margin: 0 auto 12px;
    font-size: 14px;
    color: #222222;
    opacity: 1;
}

.mob-nav:hover {
    opacity: 0.7;
}

.header_trans-fixed .mob-nav i {
    color: #fff;
}

.header_trans-fixed.header_top_bg {
    transition: background-color 300ms ease;
}

.header_trans-fixed.header_top_bg.bg-fixed-color {
    background-color: #fff;
}
.header_trans-fixed.header_top_bg.bg-fixed-color .mob-nav .line {
    background-color: #222;
}
.header_trans-fixed.header_top_bg.bg-fixed-color .logo span {
    color: #222;
}
.menu_light_text .right-menu .mob-nav .line {
    background-color: #ffffff;
}
.right-menu .topmenu.open .mob-nav .line {
    background-color: #222222;
}
.awa-top-social {
    display: inline-block;
    margin-left: 0px;
    position: relative;
    vertical-align: middle;
}

.awa-top-social .social-icon {
    display: none;
    font-size: 14px;
    color: #222222;
    opacity: 1;
    padding: 0 20px;
    cursor: pointer;
    transition: opacity 0.3s ease;
    position: relative;
    z-index: 30;
}

.header_trans-fixed .awa-top-social .social-icon {
    color: #fff;
}

.awa-top-social .social-icon:hover {
    opacity: 0.7;
}

#topmenu .awa-top-social .social {
    margin-left: 0;
}

#topmenu .social li {
    display: inline-block;
    margin-left: 12px;
}

#topmenu .awa-top-social .social li a {
    margin-left: 0;
    color: #222222;
    opacity: 1;
    transition: opacity 0.3s ease;
}

.header_trans-fixed .right-menu #topmenu .awa-top-social .social li a {
    color: #fff;
}

#topmenu .awa-top-social .social li a:hover {
    opacity: 1;
}

.header_trans-fixed .right-menu #topmenu .awa-top-social .social {
    background-color: transparent;
}

#topmenu .awa-top-social .social li {
    margin: 5px;
}

#topmenu .awa-top-social .social.active {
    visibility: visible;
    opacity: 1;
}

#topmenu .awa-top-social .social li a {
    line-height: 1.2;
}

#topmenu ul > li > ul > li > ul {
    display: none;
}

#topmenu .awa_mini_cart .product_list_widget .mini_cart_item .mini-cart-data .mini_cart_item_price {
    color: #f54ea2;
    font-size: 15px;
    font-weight: 800;
}

.mini-cart-wrapper {
    display: inline-block;
    position: relative;
    vertical-align: middle;
}

.mini-cart-wrapper .awa-shop-icon {
    text-decoration: none;
    font-size: 17px;
}

.mini-cart-wrapper .awa-shop-icon:hover::before {
    color: #f54ea2;
}

.mini-cart-wrapper .awa-shop-icon:before {
    position: relative;
    display: inline-block;
    line-height: 1;
    -webkit-transition: all 350ms ease;
    -moz-transition: all 350ms ease;
    -ms-transition: all 350ms ease;
    -o-transition: all 350ms ease;
    transition: all 350ms ease;
    color: #222222;
    font-size: 15px;
}

.mini-cart-wrapper .awa-shop-icon .cart-contents {
    display: -webkit-inline-box;
    display: -ms-inline-flexbox;
    display: inline-flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    position: absolute;
    top: -15px;
    right: -15px;
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: #f54ea2;
}

.mini-cart-wrapper .awa-shop-icon .cart-contents-count {
    font-family: "Muli";
    font-size: 10px;
    font-weight: 800;
    letter-spacing: 1.12px;
    color: #fff;
}

.awa_mini_cart {
    position: absolute;
    right: -20px;
    top: 50px;
    display: block;
    background-color: #fff;
    opacity: 0;
    visibility: hidden;
    min-width: 360px;
    padding: 23px 30px;
    text-align: center;
    transition: opacity 0.5s ease, visibility 0.5s ease;
    box-shadow: 3px 1px 20px 0 rgba(0, 0, 0, 0.07);
}
.header_trans-fixed #topmenu .awa_mini_cart .cart_list .mini_cart_item .remove_from_cart_button {
    color: #d8d8d8;
}
#topmenu .awa_mini_cart .cart_list .mini_cart_item .remove_from_cart_button {
    padding: 0;
    color: #d8d8d8;
    font-size: 30px;
    font-weight: 400;
}
#topmenu .awa_mini_cart .awa-buttons {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    margin-bottom: 30px;
}
#topmenu .awa_mini_cart .awa-buttons a {
    display: -webkit-inline-box;
    display: -ms-inline-flexbox;
    display: inline-flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    color: #222222;
    font-size: 13px;
    font-weight: 800;
    letter-spacing: .2px;
    text-transform: none;
    text-decoration: none;
}
#topmenu .awa_mini_cart .awa-buttons a:hover i {
    margin-left: 10px;
}
#topmenu .awa_mini_cart .awa-buttons a i {
    margin-left: 5px;
    color: #f54ea2;
    font-size: 10px;
    -webkit-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
}
.woocommerce-mini-cart__total {
    margin: 0;
    text-transform: none;
    font-size: 15px;
    color: #999999;
}
.woocommerce-mini-cart__total span {
    margin-left: 10px;
    color: #f54ea2;
    font-size: 18px;
    font-weight: 800;
}
.mini-cart-wrapper:hover .awa_mini_cart {
    opacity: 1;
    visibility: visible;
}

#topmenu .awa_mini_cart .product_list_widget {
    display: block;
}

#topmenu .awa_mini_cart .product_list_widget .empty {
    font-family: "Muli", sans-serif;
    font-size: 18px;
    line-height: 28px;
    letter-spacing: 1.4px;
    font-weight: 400;
    color: #fff;
}

#topmenu .awa_mini_cart .product_list_widget .mini_cart_item {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-flex-wrap: nowrap;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    padding: 0;
    padding-bottom: 23px;
    margin-bottom: 23px;
    border-bottom: 1px solid #ddd;
}

#topmenu .awa_mini_cart .product_list_widget .mini_cart_item .mini_cart_item_thumbnail {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    width: 40%;
    max-width: 70px;
    margin-top: 7px;
}

#topmenu .awa_mini_cart .product_list_widget .mini_cart_item .mini_cart_item_thumbnail a {
    padding: 0;
    display: block;
}

#topmenu .awa_mini_cart .product_list_widget .mini_cart_item .mini_cart_item_thumbnail img {
    float: none;
    max-width: 70px;
    width: 100%;
    margin-left: 0;
}

#topmenu .awa_mini_cart .product_list_widget .mini_cart_item .mini-cart-data {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-align-items: flex-start;
    -ms-flex-align: start;
    align-items: flex-start;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    width: 60%;
    padding-left: 20px;
}

#topmenu .awa_mini_cart .product_list_widget .mini_cart_item .mini-cart-data .mini_cart_item_name {
    font-family: "Muli", sans-serif;
    font-size: 15px;
    line-height: 24px;
    letter-spacing: 0.2px;
    font-weight: 800;
    color: #222;
    text-align: left;
    padding: 0;
}

#topmenu .awa_mini_cart .product_list_widget .mini_cart_item .mini-cart-data .mini_cart_item_quantity {
    font-family: "Muli", sans-serif;
    font-size: 12px;
    line-height: 20px;
    letter-spacing: 2.88px;
    font-weight: 400;
    color: #b2b2b2;
    margin-bottom: 3px;
}

#topmenu .awa_mini_cart a.button {
    margin-bottom: 0;
    letter-spacing: 1.2px;
    line-height: 20px;
    position: relative;
    display: inline-block;
    font-family: "Muli", sans-serif;
    font-weight: bold;
    box-sizing: border-box;
    padding: 18px;
    font-size: 15px;
    text-decoration: none;
    -webkit-font-smoothing: antialiased;
    color: #999;
    background-color: #eee;
    width: 100%;
    border-radius: 0;
    -webkit-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
}
#topmenu .awa_mini_cart a.button:hover {
    background-color: #f54ea2;
    color: #ffffff;
}

#topmenu .awa_mini_cart a.button:hover::after {
    right: 20px;
}

.header_trans-fixed.none {
    display: none;
}

.header_trans-fixed.header_top_bg .mini-cart-wrapper .awa-shop-icon .cart-contents-count {
    color: #fff;
}

.awa_mini_cart .product_list_widget .mini_cart_item .mini_cart_item_thumbnail img {
    height: auto;
}

.socials-mob-but {
    display: none;
}

.socials-mob-but:active,
.socials-mob-but:visited {
    opacity: 1;
}

.header_top_bg:not(.header_trans-fixed) {
    padding-bottom: 10px;
}

#topmenu .awa_mini_cart .product_list_widget .mini_cart_item .mini-cart-data .mini_cart_item_price {
    font-size: 12px;
}

.unit .mini-cart-wrapper .awa-shop-icon {
    font-size: 25px;
}

header .logo img {
    max-width: none;
    max-height: 75px;
}

header .logo img.logo-hover {
    display: none;
}
header .logo:hover {
    opacity: 1;
}

.header_trans-fixed .f-right > div:first-child::before {
    background: #fff !important;
}

@media only screen and (max-width: 1199px) {
    .awa-top-social {
        margin-left: 5px;
    }
}

@media (min-width: <?php echo esc_attr($min_mobile); ?>) {
    header .logo img.logo-mobile{
        display: none!important;
    }
    .menu_light_text.header_top_bg  .logo span,
    .menu_light_text.header_top_bg .right-menu .logo span,
    .menu_light_text.header_top_bg .right-menu #topmenu ul li a,
    .menu_light_text.header_top_bg .right-menu #topmenu .awa-shop-icon::before,
    .menu_light_text.header_top_bg .right-menu #topmenu .search-icon-wrapper i,
    .menu_light_text.header_top_bg .right-menu .socials-mob-but i {
        color: #ffffff;
    }
    .menu_light_text.header_top_bg .right-menu #topmenu .sub-menu li a {
        color: #222222;
    }
    .mob-nav-close {
        display: none;
    }
    .aside-menu .mini-cart-wrapper:hover .awa_mini_cart {
        opacity: 0;
        visibility: hidden;
    }

    .header_trans-fixed #topmenu {
        background-color: transparent;
    }

    #topmenu ul ul {
        padding: 10px 0;
    }

    .right-menu .logo{
        text-align: left;
    }

    .right-menu .logo,
    .right-menu #top-menu {
        display: table-cell;
        vertical-align: middle;
    }

    .top-menu #topmenu ul ul {
        left: -20px;
    }

    .top-menu .awa-top-social {
        margin-left: 10px;
    }

    #topmenu ul ul li {
        display: block;
        margin-bottom: 5px;
    }

    #topmenu ul ul li:last-child {
        margin-bottom: 0;
    }

    .top-menu #topmenu > ul:not(.social) > li {
        margin: 0 10px 5px 10px;
        padding: 0;
    }

    #topmenu ul li:hover > ul {
        display: block;
    }

    header:not(.full) #topmenu {
        display: block !important;
    }

    #topmenu .f-right > div {
        position: relative;
    }

    #topmenu .f-right > div:last-child::before {
        content: none;
    }

    #topmenu > ul > li > ul > li > ul {
        left: -100%;
        top: -15px;
    }

    .sub-menu li a {
        z-index: 1999;
    }

    .pr30md {
        padding-right: 30px !important;
        padding-left: 0 !important;
    }

    .right-menu {
        width: 100%;
        margin: auto;
        display: table;
        padding: 0;
    }

    .right-menu .f-right {
        float: right;
    }

    .right-menu .f-right > div {
        position: relative;
    }

    .right-menu .f-right > div:last-child::before {
        content: none;
    }

    header:not(.full) .right-menu #topmenu {
        text-align: center;
        display: table-cell !important;
        margin-top: 0;
        vertical-align: middle;
    }

    .header_trans-fixed.header_top_bg .right-menu #topmenu > ul > li > a {
        padding: 13px 0 13px;
        transform: translateZ(0);
    }

    .header_trans-fixed.header_top_bg .right-menu #topmenu > ul ul {
        top: 60px;
    }

    .header_trans-fixed.header_top_bg .right-menu #topmenu > ul ul ul {
        top: -10px;
    }

    .right-menu #topmenu ul ul {
        left: 10px;
        top: 44px;
    }

    .top-menu #topmenu ul ul {
        left: -20px;
        top: 100%;
    }

    .right-menu #topmenu > ul > li > ul > li > ul {
        left: 100%;
        top: -10px;
    }

    .top-menu #topmenu > ul > li > ul > li > ul {
        left: 100%;
        top: -10px;
    }

    .right-menu #topmenu .social {
        text-align: right;
        vertical-align: top;
    }

    .right-menu #topmenu .social li a {
        padding: 0;
        margin-left: 0;
        -webkit-transition: color 350ms ease;
        -moz-transition: color 350ms ease;
        -ms-transition: color 350ms ease;
        -o-transition: color 350ms ease;
        transition: color 350ms ease;
    }

    .right-menu #topmenu .social li a:hover {
        color: #f54ea2;
    }

    .right-menu #topmenu .social li a::after,
    .right-menu #topmenu .social li a::before {
        content: none;
    }

    .right-menu #topmenu > ul > li > a {
        position: relative;
        padding: 0;
        margin: 0 23px;
    }

    .right-menu #topmenu > ul > li.current-menu-item > a,
    .top-menu #topmenu > ul > li.current-menu-item > a,
    .right-menu #topmenu > ul > li.current-menu-parent > a,
    .top-menu #topmenu > ul > li.current-menu-parent > a {
        transition: all 0.5s ease;
    }

    .right-menu .logo img {
        max-height: 75px;
        margin: 5px auto;
    }
    .full-width-menu .right-menu .logo img {
        margin: 0;
        max-height: 77px;
    }
    .right-menu.aside-fix .logo img {
        margin: 0;
    }
    .top-menu #topmenu > ul > li:last-child > ul > li > ul {
        left: calc(-100% - 30px);
    }

    #topmenu .awa-top-social .social {
        z-index: 25;
        text-align: left;
        transition: opacity 0.3s ease;
    }

    .aside-nav {
        display: none;
    }

    .aside-menu {
        position: fixed;
        top: 0;
        left: 0;
    }

    .aside-menu .topmenu {
        position: fixed;
        top: 0;
        left: -100%;
        height: 100%;
        width: 255px !important;
        padding: 50px 0;
        margin-left: 58px;
        text-align: center;
        background-color: #fff;
        box-sizing: border-box;
        outline: 0;
        z-index: 101;
        backface-visibility: hidden;
        transition: left 0.5s cubic-bezier(0.77, 0, 0.175, 1);
    }

    .aside-menu .topmenu.active-menu {
        left: 0;
    }

    .aside-menu.active-menu {
        left: 0;
    }

    .aside-menu.active-menu .aside-nav .aside-nav-line.line-1 {
        display: none;
    }

    .aside-menu.active-menu .aside-nav .aside-nav-line.line-2 {
        top: 50%;
        transform: rotate(45deg);
    }

    .aside-menu.active-menu .aside-nav .aside-nav-line.line-3 {
        top: 50%;
        transform: rotate(-45deg);
    }

    .aside-menu .logo {
        position: absolute;
        z-index: 9999;
        top: 20px;
        left: 31px;
        padding: 20px 0;
    }

    .aside-menu .aside-nav {
        position: fixed;
        display: block;
        left: 0;
        top: 0;
        width: 58px;
        background-color: #030e28;
        height: 100%;
        z-index: 1000;
    }

    .aside-menu .aside-nav .aside-nav-line {
        position: absolute;
        top: 50%;
        left: 18px;
        display: block;
        width: 22px;
        height: 1px;
        background-color: #fff;
        transition: transform .3s ease;
    }

    .aside-menu .aside-nav .aside-nav-line.line-1 {
        transform: translateY(-6px);
    }

    .aside-menu .aside-nav .aside-nav-line.line-3 {
        transform: translateY(6px);
    }

    .aside-menu .aside-nav:hover {
        opacity: 1;
    }

    .aside-menu .aside-nav:focus {
        opacity: 1;
    }

    .aside-menu .aside-nav:hover .aside-nav-line {
        transform: rotate(45deg);
    }

    .aside-menu #topmenu {
        display: table !important;
        border-right: 1px solid #f2f2f2;
    }
    .aside-menu.aside-fix #topmenu ul.menu {
        padding-bottom: 100px;
    }
    .aside-menu #topmenu ul.menu {
        display: inline-block;
        vertical-align: middle;
        overflow-y: auto;
        width: 100%;
        max-height: 100%;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        z-index: 100;
    }

    .aside-menu #topmenu ul.menu li {
        padding: 13px 20px 13px 45px;
        position: static;
        text-align: left;
        display: block;
    }
    .aside-menu.aside-fix #topmenu ul.menu li {
        padding: 10px 15px;
        text-align: center;
    }
    .aside-menu.aside-fix #topmenu ul.menu a {
        display: inline-block;
        position: relative;
        line-height: 1.2;
    }
    .aside-menu #topmenu ul.menu a {
        display: block;
        text-align: center;
        z-index: 9999;
    }
    .aside-menu.aside-fix #topmenu ul.menu li a::before {
        content: "";
        position: absolute;
        bottom: -2px;
        left: 0;
        width: 0;
        height: 8px;
        z-index: -1;
        background-color: #ffdd65;
        -webkit-transition: width .5s ease;
        -o-transition: width .5s ease;
        transition: width .5s ease;
    }
    .aside-menu.aside-fix #topmenu ul.menu li a:hover::before,
    .aside-menu.aside-fix #topmenu ul.menu .current-menu-parent > a::before,
    .aside-menu.aside-fix #topmenu ul.menu .current-menu-item > a::before {
        width: 100%;
    }
    .aside-menu.aside-fix #topmenu ul ul li a {
        width: auto;
        padding: 0;
    }
    .aside-menu #topmenu .f-right {
        float: none;
        position: absolute;
        left: 50%;
        width: calc(100% - 35px);
        transform: translateX(-50%);
        bottom: 40px;
        z-index: 2;
    }

    .aside-menu #topmenu .f-right .awa-top-social {
        margin-left: 0;
    }

    .aside-menu #topmenu .f-right .awa-top-social li {
        margin-right: 5px;
        margin-left: 5px;
    }

    .aside-menu #topmenu .f-right .awa-top-social li {
        margin-left: 0;
        margin-right: 10px;
    }

    .aside-menu.aside-fix #topmenu .sub-menu {
        min-width: auto;
        position: static;
    }
    .aside-menu.aside-fix #topmenu .sub-menu a {
        font-weight: bold;
    }

    .aside-menu.aside-fix #topmenu::after {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 80px;
        background: #fff;
        z-index: 1;
    }

    .aside-menu.aside-fix #topmenu .f-right {
        text-align: center;
    }

    .aside-menu #topmenu > ul > li:hover ul,
    .aside-menu #topmenu > ul > li > ul > li:hover ul {
        display: none;
    }

    .aside-menu #topmenu ul > li > ul > li > ul {
        border-top: 1px solid #eee;
        border-bottom: 1px solid #eee;
    }

    header:not(.aside-menu):not(.full) #topmenu ul li.mega-menu {
        position: static;
    }

    header:not(.aside-menu):not(.full).right-menu #topmenu ul .mega-menu > ul {
        width: 100%;
        max-width: 1140px;
        left: 50%;
        top: 60px;
        padding: 45px 0 30px;
        -webkit-transform: translateX(-50%);
        -moz-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        -o-transform: translateX(-50%);
        transform: translateX(-50%);
    }


    header:not(.aside-menu):not(.full) #topmenu ul li.mega-menu:hover > ul::before {
        content: "";
        position: absolute;
        width: 5000px;
        top: 0;
        bottom: 0;
        left: -100%;
        background-color: #fff;
        box-shadow: 3px 1px 20px 0 rgba(0, 0, 0, 0.07);
        z-index: 1;
    }

    header:not(.aside-menu):not(.full) #topmenu ul li.mega-menu > ul > li {
        float: left;
        width: 25%;

    }

    header:not(.aside-menu):not(.full) #topmenu ul li.mega-menu:hover > ul > li > a {
        font-size: 18px;
        font-weight: 800;
        letter-spacing: .2px;
    }

    header:not(.aside-menu):not(.full) #topmenu ul li.mega-menu > ul > li:nth-child(1)::before {
        left: 25%;
    }

    header:not(.aside-menu):not(.full) #topmenu ul li.mega-menu > ul > li:nth-child(2)::before {
        left: 50%;
    }

    header:not(.aside-menu):not(.full) #topmenu ul li.mega-menu > ul > li:nth-child(3)::before {
        left: 75%;
    }

    header:not(.aside-menu):not(.full) #topmenu ul > li.mega-menu > ul.sub-menu > li > ul.sub-menu {
        display: block;
        position: static;
        text-align: left;
        min-width: 100%;
        box-shadow: none;
        padding: 25px 0;
        transition: all .2s ease;
    }

    header:not(.aside-menu):not(.full) #topmenu ul > li.mega-menu > ul > li > ul.sub-menu > li {
        display: block;
        padding: 8px 0;
    }
    header:not(.aside-menu):not(.full) #topmenu ul > li.mega-menu > ul > li > ul.sub-menu > li a {
        opacity: 0;
        -webkit-transform: matrix(1, 0, 0, 1, 0, 20);
        -ms-transform: matrix(1, 0, 0, 1, 0, 20);
        transform: matrix(1, 0, 0, 1, 0, 20);
        -webkit-transition: opacity .75s ease, -webkit-transform .75s ease;
        transition: opacity .75s ease, -webkit-transform .75s ease;
        -o-transition: opacity .75s ease, transform .75s ease;
        transition: opacity .75s ease, transform .75s ease;
        transition: opacity .75s ease, transform .75s ease, -webkit-transform .75s ease;
    }
    header:not(.aside-menu):not(.full).right-menu #topmenu ul .mega-menu ul li {
        position: static;
        display: block;
    }

    header.top-menu #topmenu ul li.mega-menu > ul {
        top: calc(100% - 25px);
    }

    header.top-menu #topmenu ul li.mega-menu > ul > li::before {
        display: none;
    }

    header.top-menu #topmenu ul ul {
        left: 0;
    }

    header.top-menu #topmenu ul li.mega-menu > ul > li:nth-child(1)::before,
    header.top-menu #topmenu ul li.mega-menu > ul > li:nth-child(2)::before,
    header.top-menu #topmenu ul li.mega-menu > ul > li:nth-child(3)::before {
        left: 100%;
        display: block;
        top: 0;
    }

    .top-menu .logo span {
        padding: 24px 10px;
    }

    header.top-menu .logo span {
        padding: 15px 10px;
    }

    .right-menu .logo span {
        float: left;
    }

    .top-menu #topmenu > ul:not(.social) > li {
        margin: 0 0 5px;
        padding: 0 23px;
    }

    .top-menu #topmenu > ul > li:last-child > ul > li > ul {
        left: calc(-100%);
    }

    .top-menu #topmenu > ul > li > ul > li > ul {
        left: calc(100% + 23px);
    }
}

@media (min-width: <?php echo esc_attr($min_mobile); ?>) and (max-width: 1199px) {
    .right-menu #topmenu > ul > li > a {
        margin: 0 18px;
    }
}

@media (min-width: <?php echo esc_attr($max_mobile); ?>) {

    .main-wrapper.unit .right-menu #topmenu > ul > li > a {
        margin: 0 15px;
    }
}

@media (max-width: <?php echo esc_attr($max_mobile); ?>) {

    header .logo img.main-logo:not(.logo-mobile){
        display: none!important;
    }
    header .logo img.logo-mobile{
        display: inline;
    }
    .aside-menu.static #topmenu .f-right .copy {
        display: none;
    }

    .header_top_bg > .container {
        width: 100%;
    }

    #topmenu {
        overflow-x: hidden;
    }

    .header_trans-fixed.header_top_bg .mini-cart-wrapper .awa-shop-icon .cart-contents-count {
        color: #222222;
    }

    .main-wrapper {
        width: 100%;
    }

    .main-wrapper header .logo img {
        max-height: 75px;
    }

    header {
        padding: 10px 45px;
    }

    #topmenu ul li ul {
        box-shadow: none;
        font-style: normal;
    }

    #topmenu ul {
        box-shadow: none;
        font-style: normal;
    }

    .header_top_bg > .container > .row > .col-xs-12 {
        padding: 0;
    }

    .top-menu .logo {
        margin-bottom: 0;
        margin-top: 0;
    }

    .no-padd-mob {
        padding: 0 !important;
    }
    .right-menu #topmenu .menu li.menu-item-has-children,
    #topmenu .menu li.menu-item-has-children {
        position: relative;
        text-align: left;
    }
    .right-menu #topmenu .menu li.menu-item-has-children i,
    #topmenu .menu li.menu-item-has-children i {
        position: absolute;
        top: 16px;
        right: 35px;
    }

    .right-menu #topmenu .menu li.menu-item-has-children > a,
    #topmenu .menu li.menu-item-has-children > a {
        position: relative;
        display: inline-block;
        width: 100%!important;
    }
    .mob-nav {
        display: block;
        margin: 0;
        position: absolute;
        top: 50%;
        left: 20px;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    .mob-nav i::before {
        font-size: 24px;
    }
    .sidebar-open {
        height: 100vh;
    }
    .sidebar-open .canvas-wrap {
        -webkit-transform: translateX(320px);
        -moz-transform: translateX(320px);
        -ms-transform: translateX(320px);
        -o-transform: translateX(320px);
        transform: translateX(320px);
    }
    .sidebar-open .header_top_bg {
        position: fixed;
    }
    .main-wrapper {
        -webkit-transition: -webkit-transform 0.5s ease-in-out, -webkit-transform 0.5s ease-in-out;
        transition: -webkit-transform 0.5s ease-in-out, -webkit-transform 0.5s ease-in-out;
        -o-transition: transform 0.5s ease-in-out, transform 0.5s ease-in-out;
        transition: transform 0.5s ease-in-out, transform 0.5s ease-in-out;
        transition: transform 0.5s ease-in-out, transform 0.5s ease-in-out, -webkit-transform 0.5s ease-in-out, -webkit-transform 0.5s ease-in-out;
    }
    .main-wrapper::before {
        content: '';
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.75);
        z-index: 400;
        opacity: 0;
        visibility: hidden;
        -webkit-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
    }
    .sidebar-open .main-wrapper {
        position: fixed;
        -webkit-transform: translateX(320px);
        -ms-transform: translateX(320px);
        transform: translateX(320px);
        overflow: visible;
    }
    .sidebar-open .main-wrapper::before {
        opacity: 1;
        visibility: visible;
    }
    .mob-nav-close {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        justify-content: flex-end;
        text-decoration: none;
        border-bottom: 1px solid #f1f2f3;
    }
    .mob-nav-close span {
        font-weight: 800;
        letter-spacing: .2px;
    }
    .mob-nav-close .hamburger {
        padding: 30px 35px;
        padding-left: 15px;
    }
    .mob-nav-close .line {
        display: block;
        width: 24px;
        height: 3px;
        background-color: #222222;
    }

    .mob-nav-close .line:first-of-type {
        -webkit-transform: rotate(45deg) translateY(2px);
        -moz-transform: rotate(45deg) translateY(2px);
        -ms-transform: rotate(45deg) translateY(2px);
        -o-transform: rotate(45deg) translateY(2px);
        transform: rotate(45deg) translateY(2px);
    }

    .mob-nav-close .line:last-of-type {
        -webkit-transform: rotate(-45deg) translateY(-2px);
        -moz-transform: rotate(-45deg) translateY(-2px);
        -ms-transform: rotate(-45deg) translateY(-2px);
        -o-transform: rotate(-45deg) translateY(-2px);
        transform: rotate(-45deg) translateY(-2px);
    }

    #topmenu.open {
        opacity: 1;
    }

    #topmenu {
        display: inline-block;
        overflow-y: auto;
        position: absolute;
        text-align: left;
        padding-top: 0;
        padding-bottom: 100px;
        top: 0;
        bottom: 0;
        left: 0;
        width: 320px;
        background-color: #fff;
        -webkit-transform: translateX(-320px);
        -ms-transform: translateX(-320px);
        transform: translateX(-320px);
        height: 100vh;
        z-index: 100;
    }
    .sidebar-open #topmenu {
        position: fixed;
    }
    #topmenu ul ul {
        display: none;
        position: static;
    }

    #topmenu ul.menu > li > ul > li > ul {
        display: none;
    }

    #topmenu ul.menu {
        width: 100%;
        display: inline-block;
        padding-bottom: 30px;
        background-color: #fff;
    }

    #topmenu ul.menu li {
        display: block !important;
        float: none;
        text-align: left;
        margin-bottom: 0;
    }

    #topmenu ul.menu li a::before{
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 320px;
        height: 1px;
        display: block;
        background-color: #f1f2f3;
    }
    #topmenu ul.menu li a {
        color: #222222;
        padding: 10px 35px;
        line-height: 25px;
        display: inline-block;
        width: 100%!important;
        float: none;
        transition: all 0.5s ease;
    }

    #topmenu ul.menu li a:hover {
        transition: all 0.5s ease;
    }

    /*1 level menu*/

    #topmenu > ul.menu > li > a {
        font-size: 14px;
    }

    /*2 level menu*/
    #topmenu > ul.menu > li > ul > li,
    #topmenu > ul.menu > li > ul > li > ul > li {
        padding-left: 10px;

    }

    #topmenu > ul.menu > li > ul > li > ul > li:last-child {
        margin-bottom: 20px;
    }

    #topmenu .social li a {
        line-height: 25px !important;
    }

    #topmenu .menu li a:hover,
    #topmenu .menu .current-menu-parent > a,
    #topmenu .menu .current-menu-item > a,
    #topmenu .menu .current-menu-ancestor > a {
        color: #f54ea2;
    }

    .right-menu #topmenu .social {
        display: block;
    }

    .right-menu #topmenu .social li {
        display: inline-block;
    }

    .right-menu #topmenu .social li a {
        padding: 5px;
    }

    .awa-top-social .social-icon {
        display: none;
    }

    .right-menu #topmenu .awa-top-social .social {
        position: static;
        visibility: visible;
        opacity: 1;
    }

    .header_trans-fixed.open .right-menu #topmenu .awa-top-social .social li a {
        color: #222222;
    }

    .mini-cart-wrapper {
        display: block;
        margin: 20px 10px 30px 10px;
    }

    .awa_mini_cart {
        opacity: 1;
        visibility: visible;
        position: relative;
        right: auto;
        left: 0;
        top: 10px;
        width: 100%;
        min-width: 0;
    }

    #topmenu ul li.mega-menu:hover > ul > li {
        width: 100%;
    }

    header a.logo {
        display: inline-block;
    }

    #topmenu ul li.mega-menu:hover > ul > li {
        width: auto;
    }

    #topmenu.active-socials {
        left: 0;
        right: 0;
        overflow: visible;
        opacity: 1;
        width: 100%;
    }

    #topmenu .f-right {
        display: block;
        background: #fff;
        padding: 15px;
        text-align: center;
        z-index: 9999;
        width: 100%;
        transition: all 350ms ease;
    }

    #topmenu .f-right.active-socials {
        opacity: 1;
        visibility: visible;
    }

    #topmenu .f-right.active-socials a {
        visibility: visible;
    }

    #topmenu .f-right .header_trans-fixed.open .right-menu #topmenu .awa-top-social .social li a {
        transition: none;
    }

    .socials-mob-but {
        display: block;
        margin: 0;
        position: absolute;
        top: calc(50% + -3px);
        right: 20px;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    .socials-mob-but i::before {
        font-size: 24px;
    }

    #topmenu .social .fa,
    .mini-cart-wrapper .awa-shop-icon {
        font-size: 28px;
        transition: none;
    }

    .mini-cart-wrapper .awa-shop-icon {
        margin: 5px;
    }

    .mini-cart-wrapper {
        margin: 0;
        margin-top: -3px;
    }

    .header_trans-fixed.header_top_bg.open header .socials-mob-but i,
    .header_trans-fixed #topmenu .awa-top-social .social li a,
    .header_trans-fixed .mini-cart-wrapper .awa-shop-icon::before {
        color: #222222 !important;
    }

    .header_trans-fixed.header_top_bg {
        transition: none;
    }

    .mini-cart-wrapper {
        display: inline-block;
        vertical-align: middle;
    }

    .awa_mini_cart {
        display: none;
    }

    .awa-top-social {
        vertical-align: middle;
        margin-left: 0;
    }

    .mini-cart-wrapper .awa-shop-icon:before {
        margin-top: -3px;
        font-size: 28px;
    }

    .header_trans-fixed.header_top_bg.open {
        background-color: #fff;
        position: fixed;
        z-index: 1000;
        top: 0;
        width: 100%;
    }
}

/*------------------------------------------------------*/
/*---------------------- STATIC ASIDE MENU ----------------------*/
.mCSB_container.mCS_no_scrollbar_y.mCS_y_hidden,
.mCSB_inside > .mCSB_container {
    margin-right: 0;
}

@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) {
    .static-menu {
        padding-left: 256px;
        position: relative;
    }

    .static-menu .right-menu .logo span {
        float: none;
    }

    .static-menu.woocommerce > .main-wrapper > .container {
        padding: 0 15px !important;
    }

    .static-menu.woocommerce.woocommerce-page ul.products {
        margin-top: 20px;
    }

    .static-menu.woocommerce div.product {
        margin-top: 20px;
    }

    .static-menu .awa-woocommerce-pagination .nav-links {
        padding: 30px 30px 70px;
    }

    .static-menu .main-header-testimonial {
        margin-left: auto;
        margin-right: auto;
    }

    .static-menu .single-pagination {
        padding: 15px;
    }

    .static-menu .top-banner {
        height: 500px;
    }

    .static-menu .row.single-share {
        margin-right: 0;
        margin-left: 0;
    }

    .static-menu .portfolio-single-content .izotope-container {
        margin-top: 20px;
    }

    .static-menu .pixproof-data,
    .static-menu .pixproof-data .grid__item:last-child {
        margin-top: 20px;
    }

    .static-menu .portfolio-single-content .single-pagination {
        padding: 50px 15px;
    }

    .static-menu .banner-slider .page-view {
        max-width: 100%;
    }

    .static-menu .portfolio-single-content p,
    .static-menu .portfolio-single-content h1,
    .static-menu .portfolio-single-content h2,
    .static-menu .portfolio-single-content h3,
    .static-menu .portfolio-single-content h4,
    .static-menu .portfolio-single-content h5,
    .static-menu .portfolio-single-content h6 {
        padding: 0 15px;
    }

    .static-menu .portfolio-single-content .row.gallery-single {
        margin-right: 0;
        margin-left: 0;
    }

    .static-menu .swiper-container-split .swiper-slide .slide-item.slide-text-left .wrap-slide-text {
        padding-left: 190px;
    }

    .static-menu .vc_row:not([data-vc-stretch-content="true"]) {
        padding-left: 0 !important;
        padding-right: 0 !important;
    }

    .static-menu .vc_row[data-vc-full-width] {
        max-width: calc(100% + 30px) !important;
        left: 0 !important;
    }

    .static-menu .top-banner .content {
        padding: 0 15px;
    }

    .static-menu .flow-slider .swiper-container {
        width: 120vw;
    }

    .static-menu .exhibition-wrap .container-wrap {
        max-width: 100%;
    }

    .static-menu .exhibition-wrap .slide {
        max-width: calc(70vw - 90px);
    }

    .static-menu #footer.fix-bottom {
        max-width: calc(100% - 254px);
        left: 254px;
    }

    .static-menu .header_top_bg {
        padding-bottom: 0;
    }

    .aside-menu.static {
        max-width: 290px;
        left: 0;
    }

    .aside-menu.static .aside-nav {
        display: none;
    }

    .aside-menu.static #topmenu ul.menu {
        overflow-y: visible;
        transform: translate(-50%, -50%);
    }

    .aside-menu.static #topmenu {
        box-shadow: 3px 1px 20px 0 rgba(0, 0, 0, 0.12);
        left: 0 !important;
        margin-left: 0;
        vertical-align: top;
        border-right: none;
    }

    .aside-menu.static #topmenu .sub-menu {
        margin-left: 0;
        left: 100%;
        top: 50%;
        background-color: #fff;
        width: 100% !important;
        transform: translateY(-50%);
        box-shadow: 3px 1px 20px 0 rgba(0, 0, 0, 0.12);
    }
    .aside-menu.static #topmenu .sub-menu .sub-menu {
        left: 99%;
    }

    .aside-menu.static #topmenu li:hover > .sub-menu {
        display: none;
    }

    .aside-menu.static #topmenu .menu li a {
        text-align: left;
        font-size: 15px;
        font-weight: 800;
        display: inline-block;
        letter-spacing: .2px;
        margin: 0;
        width: auto;
        line-height: 1.1;
        padding: 0 2px 0 0;
    }

    .aside-menu.static #topmenu .menu li a:hover {
        margin-left: 10px !important;
        position: relative;
    }
    .aside-menu.static #topmenu .menu .current-menu-item  > a:hover,
    .aside-menu.static #topmenu .menu .current-menu-parent > a:hover {
        margin-left: 0!important;
    }

    .aside-menu.static #topmenu .menu .current-menu-parent li a,
    .aside-menu.static #topmenu .menu .current-menu-item li a {
        margin-left: 0 !important;
    }

    .aside-menu.static #topmenu .current-menu-parent > a::before,
    .aside-menu.static #topmenu .current-menu-item > a::before {
        content: "";
        position: absolute;
        bottom: -2px;
        left: 0;
        width: 100%;
        height: 8px;
        z-index: -1;
        background-color: #ffdd65;
    }

    .aside-menu.static #topmenu .f-right {
        text-align: left;
        left: 0;
        -webkit-transform: none;
        -moz-transform: none;
        -ms-transform: none;
        -o-transform: none;
        transform: none;
        width: 100%;
        padding-left: 45px;
    }

    .aside-menu.static #topmenu .f-right .copy {
        text-transform: none;
        font-size: 14px;
        line-height: 1.57;
        color: #999999;
        margin-top: 35px;
    }

    .aside-menu.static .logo {
        left: 0;
        width: 100%;
        top: 0;
        padding-top: 40px;
        text-align: left;
        padding-left: 45px;
    }

    .aside-menu.static .logo span {
        font-size: 30px;
        line-height: 1;
    }

    .aside-menu.static .logo img {
        max-width: 100%;
    }
}

@media only screen and (min-width: 1650px) {
    .static-menu .vc_row:not([data-vc-stretch-content="true"]) {
        padding-left: 7% !important;
        padding-right: 7% !important;
    }
}

@media only screen and (min-width: 1199px) and (max-width: 1375px) {
    .static-menu .pricing-item {
        padding: 60px 40px;
    }

    .static-menu .pricing-item .mask-image {
        min-width: 150px;
        width: 150px;
    }
}

@media only screen and (min-width: 1200px) and (max-width: 1275px) {
    .static-menu .pricing-item .mask-image {
        min-width: 130px;
        width: 130px;
    }
}
@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) and (max-width: 1460px) {
    .static-menu .about-section {
        padding: 0 30px;
        overflow: hidden;
    }
    .static-menu .headings-wrap,
    .static-menu .awa-post-list-1,
    .static-menu .awa-post-list-2,
    .static-menu .awa-portfolio-2,
    .static-menu .awa-portfolio-3,
    .static-menu .awa-portfolio-urban {
        padding: 0 15px;
    }
    .static-menu .contacts-info-wrap {
        padding: 0 15px 15px;
    }
    .static-menu .vc_row.pad-fix {
        padding-right: 15px!important;
        padding-left: 15px!important;
    }
}
@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) and (max-width: 1375px) {
    .static-menu .outer-album-swiper .album-text-block,
    .static-menu .outer-album-swiper .right-content {
        max-width: 260px;
    }
}

@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) and (max-width: 1350px) {
    .static-menu .contacts-info-wrap.style3 .content {
        padding: 100px 20px;
    }

    .static-menu .swiper3-container.carousel-albums .swiper3-button-prev {
        left: 30px;
    }

    .static-menu .swiper3-container.carousel-albums .swiper3-button-prev:hover {
        left: 20px;
    }

    .static-menu .swiper3-container.carousel-albums .swiper3-button-next {
        right: 30px;
    }

    .static-menu .swiper3-container.carousel-albums .swiper3-button-next:hover {
        right: 20px;
    }
}

@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) and (max-width: 1200px) {
    .static-menu.single-product .product .woocommerce-Reviews #comments, .static-menu.awa_product_detail .product .woocommerce-Reviews #comments {
        width: 60%;
    }

    .static-menu.single-product .product .woocommerce-Reviews #review_form_wrapper, .static-menu.awa_product_detail .product .woocommerce-Reviews #review_form_wrapper {
        width: 40%;
    }

    .static-menu .coming-soon .svg .count {
        font-size: 115px;
    }

    .static-menu .client-wrap {
        width: 50%;
    }

    .static-menu .info-block-parallax-wrap .content-wrap {
        padding: 70px 20px 70px;
    }
}

@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) and (max-width: 1100px) {
    .static-menu .top-banner {
        height: 300px;
    }

    .static-menu .top-banner.center_content {
        min-height: 300px;
    }

    .static-menu .fragment-wrapper .fragment-block .fragment-text .wrap-frag-text .title {
        font-size: 50px;
        line-height: 55px;
    }

    .static-menu .swiper-container-vert-slider .swiper-slide .container .wrap-text {
        max-width: calc(100% - 40px);
    }

    .static-menu .swiper-container-vert-slider .swiper-slide .container .wrap-text .title {
        font-size: 60px;
        letter-spacing: 8px;
    }

    .static-menu .portfolio-slider-wrapper.slider_classic .content-wrap .portfolio-title {
        font-size: 50px;
        letter-spacing: 7px;
    }

    .static-menu .portfolio-single-content .gallery-single.infinite_full_gallery .item-single {
        width: 33.33%;
    }

    .static-menu .portfolio.grid .item {
        width: 50% !important;
    }

    .static-menu .flow-slider .flow-title {
        font-size: 60px;
    }
}

/*------------------------------------------------------*/
/*---------------------- ASIDE MENU VERTICAL LOGO ----------------------*/

@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) {
    header.aside-menu {
        padding: 0;
    }

    .aside-menu.vertical .logo {
        position: fixed;
        top: auto;
        bottom: -35px;
        left: 0;
        transform-origin: left top 0;
        transform: rotate(-90deg);
        padding: 0;
        height: 58px;

    span,
    img {
        max-height: 58px;
        margin: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
}

}

/*------------------------------------------------------*/
/*---------------------- LEFT ALIGN MENU ----------------------*/
@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) {
    .full-width-menu .right-menu.left {
        padding: 0;
    }
    .right-menu.left {
        padding: 0 85px;
    }
    .unit .right-menu.left {
        padding: 0 15px;
    }
    .right-menu.left .site-search {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 100;
        background-color: rgba(255, 255, 255, .9);
        overflow-x: hidden;
        overflow-y: auto;
        opacity: 0;
        visibility: hidden;
        -webkit-transition: opacity .7s ease, visibility .7s ease;
        -o-transition: opacity .7s ease, visibility .7s ease;
        transition: opacity .7s ease, visibility .7s ease;
    }
    .right-menu.left .site-search.open {
        opacity: 1;
        visibility: visible;
    }
    .right-menu.left .site-search .form-container {
        position: relative;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }
    .right-menu.left .site-search .form-container .input-group {
        width: 100%;
    }
    .right-menu.left .site-search .form-container .input-group input {
        font-size: 18px;
    }
    .right-menu.left .site-search .close-search {
        position: absolute;
        top: 80px;
        right: 80px;
        width: 30px;
        height: 30px;
    }
    .right-menu.left .site-search .line {
        width: 18px;
        height: 3px;
        background-color: #222222;
        display: block;
        margin: 2px auto;
        -webkit-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
    .right-menu.left .site-search .line:nth-of-type(1) {
        -webkit-transform: translateY(1px) rotate(45deg);
        -ms-transform: translateY(1px) rotate(45deg);
        -o-transform: translateY(1px) rotate(45deg);
        transform: translateY(1px) rotate(45deg);
    }
    .right-menu.left .site-search .line:nth-of-type(2) {
        -webkit-transform: translateY(-4px) rotate(-45deg);
        -ms-transform: translateY(-4px) rotate(-45deg);
        -o-transform: translateY(-4px) rotate(-45deg);
        transform: translateY(-4px) rotate(-45deg);
    }
    .right-menu.left .f-right {
        float: none;
    }
    .header_trans-fixed.header_top_bg .right-menu.left #topmenu  ul  li  a {
        padding: 0;
    }
    .right-menu.left #topmenu .menu li a {
        font-size: 15px;
        font-weight: 800;
        letter-spacing: .2px;
        line-height: 1.1;
        z-index: 1;
    }

    .right-menu.left #topmenu .menu > li {
        padding: 30px 0;
    }

    .right-menu.left #topmenu .sub-menu {
        top: 75px;
        left: -35px;
        min-width: 270px;
        padding: 30px 0;
        background-color: #ffffff;
        -webkit-box-shadow: 3px 1px 20px 0 rgba(0, 0, 0, 0.07);
        box-shadow: 3px 1px 20px 0 rgba(0, 0, 0, 0.07);
        opacity: 0;
        visibility: hidden;
        -webkit-transition: opacity .5s ease, visibility .5s ease;
        -o-transition: opacity .5s ease, visibility .5s ease;
        transition: opacity .5s ease, visibility .5s ease;
        display: block;
    }
    .right-menu.left #topmenu .menu li:hover > ul {
        opacity: 1;
        visibility: visible;
    }
    .right-menu.left #topmenu .menu .mega-menu:hover > ul > li > ul {
        opacity: 1;
        visibility: visible;
    }
    .right-menu.left #topmenu .menu > li ul a {
        opacity: 0;
        -webkit-transform: matrix(1, 0, 0, 1, 0, 20);
        -ms-transform: matrix(1, 0, 0, 1, 0, 20);
        transform: matrix(1, 0, 0, 1, 0, 20);
        -webkit-transition: opacity .75s ease, -webkit-transform .75s ease;
        transition: opacity .75s ease, -webkit-transform .75s ease;
        -o-transition: opacity .75s ease, transform .75s ease;
        transition: opacity .75s ease, transform .75s ease;
        transition: opacity .75s ease, transform .75s ease, -webkit-transform .75s ease;
    }

    .right-menu.left .topmenu .sub-menu .not-hover a:before {
        display: none;
    }
    .right-menu.left #topmenu .menu > li:hover ul a,
    .right-menu.left #topmenu .menu > li.mega-menu:hover ul > li > ul.sub-menu > li a {
        opacity: 1;
        -webkit-transform: matrix(1, 0, 0, 1, 0, 0);
        -ms-transform: matrix(1, 0, 0, 1, 0, 0);
        transform: matrix(1, 0, 0, 1, 0, 0);
    }

    .right-menu.left #topmenu .sub-menu .sub-menu {
        top: 0;
        left: 100%;
        padding: 40px 15px;
    }

    .right-menu.left #topmenu .menu li:last-of-type .sub-menu .sub-menu,
    .right-menu.left #topmenu .menu li:nth-last-of-type(2) .sub-menu .sub-menu,
    .right-menu.left #topmenu .menu li:nth-last-of-type(3) .sub-menu .sub-menu {
        left: -100%;
    }

    .right-menu.left #topmenu .sub-menu li {
        padding: 8px 35px;
        text-align: left;
    }

    .right-menu.left #topmenu .sub-menu li a {
        width: auto;
        display: inline-block;
        padding: 0;
        font-weight: 600;
    }

    .right-menu.left #topmenu li a::before {
        content: "";
        position: absolute;
        bottom: -2px;
        left: 0;
        width: 0;
        height: 8px;
        z-index: -1;
        background-color: #ffdd65;
        -webkit-transition: width .3s ease;
        -o-transition: width .3s ease;
        transition: width .3s ease;
    }

    .right-menu.left #topmenu .current-menu-parent > a,
    .right-menu.left #topmenu .current-menu-item > a {
        position: relative;
    }

    .right-menu.left #topmenu .menu li a:hover::before,
    .right-menu.left #topmenu .current-menu-parent > a::before,
    .right-menu.left #topmenu .current-menu-item > a::before,
    .right-menu.left #topmenu .current-menu-ancestor > a::before {
        width: 100%;
    }

    .right-menu.left .logo span {
        margin-top: 2px;
    }

    .right-menu.left #topmenu {
        padding-left: 23px;
        text-align: right;
    }

    .right-menu.left #topmenu > ul > li > a {
        margin: 0 18px 0 0;
    }

    .right-menu.left #topmenu .search-icon-wrapper {
        margin-left: 30px;
        cursor: pointer;
    }
    .right-menu.left #topmenu .mini-cart-wrapper {
        margin-left: 30px;
    }
    .search-form .input-group::after {
        display: block;
        position: absolute;
        bottom: 0;
        content: "";
        height: 3px;
        width: 50%;
        background-color: #f54ea2;
    }
    .search-form input {
        width: 100%;
        border: 0;
        border-bottom: 3px solid rgba(34, 34, 34, .2);
        background-color: transparent;
        color: #999999;
        font-size: 15px;
        padding: 14px 0;
    }
    
    .header_top_bg.center-menu .right-menu:not(.full) #topmenu {
        display: flex !important;
        align-items: center;
    }

    .header_top_bg.center-menu #topmenu > ul {
        width: 100%;
        text-align: center;
    }

    .header_top_bg.center-menu .right-menu.left .f-right {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .header_top_bg.center-menu .right-menu.left {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }
}

@media only screen and (max-width: <?php echo esc_attr($max_mobile); ?>) {
    header {
        position: static;
    }
    #topmenu ul.menu {
        max-height: 300000px !important;
    }
}

/*------------------------------------------------------*/
/*---------------------- COMPACT MENU ----------------------*/
@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) {
    .right-menu.compact .mob-nav {
        display: table-cell;
        vertical-align: middle;
        padding: 30px;
        width: 12px;
        font-size: 20px;
    }

    .right-menu.compact .mob-nav .fa-times {
        font-size: 22px;
    }

    .right-menu.compact #topmenu {
        opacity: 0;
        visibility: hidden;
        transition: all 350ms ease;
    }

    .right-menu.compact #topmenu.open {
        opacity: 1;
        visibility: visible;
    }

    .header_trans-fixed.header_top_bg.open .right-menu.compact #topmenu ul li a,
    .header_trans-fixed.header_top_bg.open .right-menu.compact .logo span,
    .header_trans-fixed.header_top_bg.open .right-menu.compact .mob-nav i {
        color: #fff;
    }
}

/*------------------------------------------------------*/
/*---------------------- FULL SCREEN MENU ----------------------*/

@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) {
    .full {
        padding: 0 85px!important;
        text-align: left;
    }
    .right-menu.full .mob-nav {
        width: auto;
        padding: 30px 0;
        text-align: right;
        text-decoration: none;
    }
    .right-menu.full .mob-nav > span {
        display: none;
        margin-right: 10px;
        color: #222222;
        font-size: 24px;
        font-weight: 800;
    }
    .right-menu.full .mob-nav .line {
        width: 18px;
        height: 3px;
        background-color: #222222;
        display: block;
        float: left;
        margin: 2px auto;
        -webkit-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
    .menu_light_text.header_top_bg .right-menu.full .mob-nav .line {
        background-color: #fff;
    }
    .header_trans-fixed.header_top_bg.bg-fixed-color .mob-nav .line,
    .menu_light_text.header_trans-fixed.header_top_bg.bg-fixed-color .mob-nav .line,
    .menu_light_text.header_top_bg .right-menu.full .mob-nav.active .line{
        background-color: #222;
    }
    .right-menu.full .mob-nav.active > span {
        display: inline-block;
    }
    .right-menu.full .mob-nav .hamburger {
        display: inline-block;
        width: 20px;
    }
    .right-menu.full .mob-nav.active .line {
        margin: 0;
        background-color: #222222;
    }
    .right-menu.full .mob-nav.active .line:nth-of-type(2) {
        opacity: 0;
    }
    .right-menu.full .mob-nav.active .line:nth-of-type(1) {
        width: 24px;
        -webkit-transform: translateY(1px) rotate(45deg);
        -ms-transform: translateY(1px) rotate(45deg);
        -o-transform: translateY(1px) rotate(45deg);
        transform: translateY(1px) rotate(45deg);
    }
    .right-menu.full .mob-nav.active .line:nth-of-type(3) {
        width: 24px;
        -webkit-transform: translateY(-5px) rotate(-45deg);
        -ms-transform: translateY(-5px) rotate(-45deg);
        -o-transform: translateY(-5px) rotate(-45deg);
        transform: translateY(-5px) rotate(-45deg);
    }
    .right-menu.full .mob-nav .line:nth-of-type(2) {
        width: 24px;
    }
    .right-menu.full #topmenu .full-menu-wrap {
        position: absolute;
        top: 10%;
        left: 0;
        width: 100%;
        padding: 30px 100px;
        text-align: left;
        height: 100%;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }

    .right-menu.full #topmenu {
        margin-top: 0;
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        display: none;
        transition: none;
        width: 100%;
        opacity: 1;
        visibility: visible;
        height: 100vh;
        background: rgba(255, 255, 255, .95);
        overflow-y: auto;
        z-index: 90;
    }
    .right-menu.full #topmenu.open {
        opacity: 1;
        visibility: visible;
    }
    .right-menu.full #topmenu li {
        text-align: left;
    }
    .header_top_bg .right-menu.full #topmenu ul li a {
        color: #222222;
    }
    .right-menu.full #topmenu ul.menu li {
        overflow: hidden;
    }
    .right-menu.full #topmenu ul.menu li a {
        position: relative;
        display: inline-block;
        padding: 0;
        font-size: 30px;
        text-align: left;
        line-height: 2;
        -webkit-transform: translateY(100%);
        -ms-transform: translateY(100%);
        transform: translateY(100%);
        -webkit-transition: -webkit-transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        transition: -webkit-transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        -o-transition: transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        transition: transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        transition: transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160), -webkit-transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
    }
    .right-menu.full .info-wrap .additional > div,
    .right-menu.full .info-wrap .search > div {
        overflow: hidden;
    }
    .right-menu.full .info-wrap .additional div *,
    .right-menu.full .info-wrap .search div * {
        -webkit-transform: translateY(130%);
        -ms-transform: translateY(130%);
        transform: translateY(130%);
        -webkit-transition: -webkit-transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        transition: -webkit-transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        -o-transition: transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        transition: transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        transition: transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160), -webkit-transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
    }
    .right-menu.full .copy {
        opacity: 0;
        webkit-transform: translateY(140%);
        -ms-transform: translateY(140%);
        transform: translateY(140%);
        -webkit-transition: opacity 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160), -webkit-transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        transition: opacity 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160), -webkit-transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        -o-transition: transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160), opacity 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        transition: transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160), opacity 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
        transition: transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160), opacity 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160), -webkit-transform 500ms cubic-bezier(0.510, -0.015, 0.860, 0.160);
    }
    .right-menu.full #topmenu.open ul.menu li a,
    .right-menu.full .open .info-wrap .additional div *,
    .right-menu.full .open .info-wrap .search div * {
        -webkit-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
    }
    .right-menu.full .open .copy {
        -webkit-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
        opacity: 1;
    }
    .right-menu.full #topmenu ul.menu li .sub-menu {
        display: none;
        transition: none;
    }
    .right-menu.full #topmenu li a::before {
        content: "";
        position: absolute;
        bottom: 8px;
        left: 0;
        width: 0;
        height: 18px;
        z-index: -1;
        background-color: #ffdd65;
        transition: width .3s ease;
    }

    .right-menu.full #topmenu .current-menu-parent > a,
    .right-menu.full #topmenu .current-menu-item > a {
        position: relative;
    }

    .right-menu.full #topmenu .menu li a:hover::before,
    .right-menu.full #topmenu .current-menu-parent > a::before,
    .right-menu.full #topmenu .current-menu-item > a::before,
    .right-menu.full #topmenu .current-menu-ancestor > a::before {
        width: 100%;
    }

    header.full #topmenu ul li.mega-menu:hover > ul {
        padding-top: 10px;
    }

    .right-menu.full #topmenu ul li::before {
        display: none;
    }

    .right-menu.full #topmenu ul ul li {
        display: block !important;
        float: none !important;
        width: 100% !important;
    }

    .right-menu.full #topmenu .f-right li {
        display: inline-block;
    }

    .right-menu.full #topmenu .f-right {
        display: block;
        float: none;
        margin-top: 30px !important;
    }

    .right-menu.full #topmenu ul li {
        display: block;
    }

    .right-menu.full #topmenu .mob-nav {
        position: relative;
        z-index: 9000;
    }
    .right-menu.full #topmenu .menu {
        margin-bottom: 30px;
    }
    .right-menu.full .sub-menu {
        position: static !important;
        -webkit-transform: none;
        -moz-transform: none;
        -ms-transform: none;
        -o-transform: none;
        transform: none;
    }
    .right-menu.full #topmenu ul ul {
        display: block;
        padding-left: 30px;
    }
    .right-menu.full #topmenu ul.menu ul li a {
        display: inline-block;
        width: auto;
        font-size: 18px;
    }
    .right-menu.full #topmenu ul.menu ul li a::before {
        height: 8px;
    }
    .right-menu.full .whizz_mini_cart {
        display: none;
    }

    .right-menu.full .mob-nav {
        position: relative;
        z-index: 1000;
    }
    .right-menu.full .info-wrap {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
        margin-bottom: 30px;
    }
    .right-menu.full .copy {
        text-align: right;
        text-transform: capitalize;
    }
    .right-menu.full .info-wrap .additional {
        width: 40%;
    }
    .right-menu.full .info-wrap .search {
        width: 30%;
    }
    .right-menu.full .info-wrap .additional h3,
    .right-menu.full .info-wrap .search {
        font-size: 30px;
        font-weight: 800;
        line-height: normal;
    }
    .right-menu.full .info-wrap .additional p {
        margin: 0;
        font-size: 15px;
        line-height: 2.67;
    }
    .right-menu.full .info-wrap .search .input-group {
        width: 100%;
    }
    .right-menu.full .info-wrap .search .input-group::before {
        position: absolute;
        top: 50%;
        right: 10px;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        font-family: "FontAwesome";
        font-size: 15px;
        color: #222222;
        content: "\f002";
    }
    .right-menu.full .info-wrap .search input {
        color: #999999;
        font-size: 15px;
        font-weight: normal;
    }
    .right-menu.full .info-wrap .search .col-lg-12 {
        padding: 0;
    }
    .right-menu.full .copy {
        color: #222;
        font-size: 15px;
        opacity: .6;
    }
}

@media only screen and (max-width: <?php echo esc_attr($max_mobile); ?>) {
    .header_top_bg{
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 100;
    }
    .right-menu.full #topmenu {
        background-color: #ffffff;
    }
    .right-menu.full #topmenu .sub-menu li {
        padding-left: 10px;
    }
    .right-menu.full #topmenu .full-menu-wrap {
        position: static;
        padding: 0;
    }
    .right-menu.full .mob-nav {
        width: auto;
        text-align: right;
        text-decoration: none;
    }
    .right-menu.full .mob-nav > span {
        display: none;
        margin-right: 10px;
        color: #222222;
        font-size: 24px;
        font-weight: 800;
    }
    .right-menu .mob-nav .line {
        width: 18px;
        height: 3px;
        background-color: #222222;
        display: block;
        float: left;
        margin: 2px auto;
        -webkit-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }

    .right-menu .mob-nav .hamburger {
        display: inline-block;
        width: 20px;
    }
    .right-menu .mob-nav.active .line {
        margin: 0;
        background-color: #222222;
    }
    .right-menu .mob-nav.active .line:nth-of-type(2) {
        opacity: 0;
    }
    .right-menu .mob-nav.active .line:nth-of-type(1) {
        width: 24px;
        -webkit-transform: translateY(2px) rotate(45deg);
        -ms-transform: translateY(2px) rotate(45deg);
        -o-transform: translateY(2px) rotate(45deg);
        transform: translateY(2px) rotate(45deg);
    }
    .right-menu .mob-nav.active .line:nth-of-type(3) {
        width: 24px;
        -webkit-transform: translateY(-4px) rotate(-45deg);
        -ms-transform: translateY(-4px) rotate(-45deg);
        -o-transform: translateY(-4px) rotate(-45deg);
        transform: translateY(-4px) rotate(-45deg);
    }
    .right-menu .mob-nav .line:nth-of-type(2) {
        width: 24px;
    }
    .right-menu.full .info-wrap {
        padding: 0 15px 0 35px;
        margin-bottom: 30px;
    }
    .right-menu.full .info-wrap .additional {
        margin-bottom: 30px;
    }
    .right-menu.full .copy {
        text-align: right;
        text-transform: capitalize;
    }
    .right-menu.full .info-wrap .additional h3,
    .right-menu.full .info-wrap .search {
        font-size: 24px;
        font-weight: 800;
        line-height: normal;
    }
    .right-menu.full .info-wrap .additional p {
        margin: 0;
        font-size: 15px;
        line-height: 2.67;
    }
    .right-menu.full .info-wrap .search .input-group {
        width: 100%;
        max-width: 500px;
    }
    .right-menu.full .info-wrap .search .input-group::before {
        position: absolute;
        top: 50%;
        right: 10px;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        font-family: "FontAwesome";
        font-size: 15px;
        color: #222222;
        content: "\f002";
    }
    .right-menu.full .info-wrap .search input {
        color: #999999;
        font-size: 15px;
        font-weight: normal;
    }
    .right-menu.full .info-wrap .search .col-lg-12 {
        padding: 0;
    }
    .right-menu.full .copy {
        padding: 0 15px;
        color: #999999;
        font-size: 15px;
    }
    .search-form .input-group::after {
        display: block;
        position: absolute;
        bottom: 0;
        content: "";
        height: 3px;
        width: 50%;
        background-color: #f54ea2;
    }
    .search-form input {
        width: 100%;
        border: 0;
        border-bottom: 3px solid rgba(34, 34, 34, .2);
        background-color: transparent;
        color: #999999;
        font-size: 15px;
        padding: 14px 0;
    }
    .search-icon-wrapper {
        display: block;
        position: relative;
        margin-top: 30px;
    }
    .search-icon-wrapper i {
        position: absolute;
        top: 50%;
        right: 20px;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        -o-transform: translateY(-50%);
        transform: translateY(-50%);
    }
    .search-icon-wrapper .input-group {
        width: 100%;
    }
    .right-menu.full .socials-mob-but,
    .right-menu.full .copy,
    .right-menu.full .additional {
        display: none;
    }
}

<?php
$style = '';
///HEADER LOGO//
if ( cs_get_option('site_logo') == 'txtlogo' ) {
    //Header logo text
    if ( cs_get_option('text_logo_style') == 'custom' ) {

        $style .= 'a.logo span{';
        $style .=  cs_get_option('text_logo_color') && cs_get_option('text_logo_color') !== '#fff' ? 'color:' . cs_get_option('text_logo_color') . ' !important;' : '';
        $style .=  cs_get_option('text_logo_width') ? 'width:' . cs_get_option('text_logo_width') . ' !important;' : '';
        $style .=  cs_get_option('text_logo_font_size') ? 'font-size:' . cs_get_option('text_logo_font_size') . ' !important;' : '';
        $style .= '}';
    }

} else {
    //Header logo image
    if ( cs_get_option('img_logo_style') == 'custom' ) {
        $style .= '.logo img {';
        if (cs_get_option('img_logo_width')) {
            $logo_width = is_integer(cs_get_option('img_logo_width')) ? cs_get_option('img_logo_width') . 'px' : cs_get_option('img_logo_width');
             $style .=  cs_get_option('img_logo_width') ? 'width:' . esc_attr($logo_width) . ' !important;' : '';
        }
        if (cs_get_option('img_logo_height')) {
            $logo_height = is_integer(cs_get_option('img_logo_height')) ? cs_get_option('img_logo_height') . 'px' : cs_get_option('img_logo_height');
             $style .=  cs_get_option('img_logo_height') ? 'height:' . esc_attr( $logo_height ) . ' !important;' : '';
             $style .=  cs_get_option('img_logo_height') ? 'max-height:' . cs_get_option('img_logo_height') . ' !important;' : '';
        }
        $style .= '}';
    }
}
echo esc_html($style);

$post_id = isset($_GET['post']) && is_numeric($_GET['post']) ? $_GET['post'] : '' ;

if(!empty($post_id)){
 $meta_data = get_post_meta( $post_id, '_custom_page_options', true );

if (isset($meta_data['footer_color']) && !empty($meta_data['footer_color'])) { ?>
.page-id-<?php echo esc_attr($post_id); ?> #footer{
    background-color: <?php echo esc_html($meta_data['footer_color']) ?>;
}
<?php }

if (isset($meta_data['header_scroll_bg']) && !empty($meta_data['header_scroll_bg'])) { ?>
.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color{
    background-color: <?php echo esc_html($meta_data['header_scroll_bg']) ?>;
}
<?php }
if (isset($meta_data['header_scroll_text']) && !empty($meta_data['header_scroll_text'])) { ?>

.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color #topmenu:not(.open) ul li a,
.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color #topmenu .search-icon-wrapper i,
.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color #topmenu .mini-cart-wrapper .awa-shop-icon::before,
.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color #topmenu .awa-top-social .social li a,
.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color .header_top_bg .right-menu.full #topmenu ul li a,
.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color .right-menu .logo span,
.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color .top-menu .logo span,
.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color .aside-menu.static #topmenu .f-right .copy {
    color: <?php echo esc_html($meta_data['header_scroll_text']) ?>;
}

.page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color .right-menu.full .mob-nav:not(.active) .line {
    background-color: <?php echo esc_html($meta_data['header_scroll_text']) ?>;
}

@media only screen and (max-width: <?php echo esc_attr($max_mobile); ?>) {
    .page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color .mob-nav-close,
    .page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color #topmenu ul.menu li a {
        color: <?php echo esc_html($meta_data['header_scroll_text']) ?>;
    }

    .page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color .mob-nav-close .line,
    .page-id-<?php echo esc_attr($post_id); ?> .header_trans-fixed.header_top_bg.bg-fixed-color .right-menu .mob-nav .line {
        background-color: <?php echo esc_html($meta_data['header_scroll_text']) ?>;
    }
}
<?php }

 if(!empty($meta_data['padding_desktop'])){ ?>
.page-id-<?php echo esc_attr($post_id); ?> .padding-desc,
.page-id-<?php echo esc_attr($post_id); ?> .padding-desc .vc_row,
.page-id-<?php echo esc_attr($post_id); ?> .padding-desc + #footer > div {
    padding-right: <?php echo esc_attr($meta_data['padding_desktop']); ?>;
    padding-left: <?php echo esc_attr($meta_data['padding_desktop']); ?>;
}

<?php }

 if(!empty($meta_data['padding_mobile'])){ ?>

@media only screen and (max-width: 767px) {
    .page-id-<?php echo esc_attr($post_id); ?> .padding-mob,
    .page-id-<?php echo esc_attr($post_id); ?> .padding-mob .vc_row,
    .page-id-<?php echo esc_attr($post_id); ?> .padding-mob + #footer > div {
        padding-right: <?php echo esc_attr($meta_data['padding_mobile']); ?>;
        padding-left: <?php echo esc_attr($meta_data['padding_mobile']); ?>;
    }
}

@media (min-width: 768px) {
    .right-menu {
        width: 100%;
        margin: 0;
        max-width: 100%;
    }

    .header_top_bg .col-xs-12 {
        padding: 0;
    }
}

<?php }
}

$preloaderSize_d = cs_get_option('preloader_size_d');
$preloaderSize_t = cs_get_option('preloader_size_t');
$preloaderSize_m = cs_get_option('preloader_size_m');

if(isset($preloaderSize_d) && $preloaderSize_d){ ?>
.preloader-svg svg {
    height: <?php echo esc_html($preloaderSize_d); ?>px !important;
}

<?php }

if(isset($preloaderSize_t) && $preloaderSize_t){ ?>
@media only screen and (max-width: 991px) {
    .preloader-svg svg {
        height: <?php echo esc_html($preloaderSize_t); ?>px !important;
    }
}

<?php }

if(isset($preloaderSize_m) && $preloaderSize_m){ ?>
@media only screen and (max-width: 767px) {
    .preloader-svg svg {
        height: <?php echo esc_html($preloaderSize_m); ?>px !important;
    }
}

<?php } ?>

/**** WHITE VERSION  ****/

<?php

if(cs_get_option('change_colors')){
if (cs_get_option( 'menu_bg_color') && cs_get_option('menu_bg_color') !== '#ffffff') { ?>
.header_top_bg,
#topmenu,
.aside-menu.aside-fix #topmenu::after {
    background-color: <?php echo esc_html(cs_get_option( 'menu_bg_color')) ?>;
}
@media only screen and (max-width: <?php echo esc_attr($max_mobile); ?>) {
    .right-menu.full #topmenu,
    #topmenu ul.menu,
    #topmenu .f-right {
        background-color: <?php echo esc_html(cs_get_option( 'menu_bg_color')) ?>;
    }
}


<?php }
if (cs_get_option( 'menu_font_color') && cs_get_option('menu_font_color') !== '#222222' ) { ?>
#topmenu ul li a,
#topmenu .search-icon-wrapper i,
.mini-cart-wrapper .awa-shop-icon:before,
#topmenu .awa-top-social .social li a,
.header_top_bg .right-menu.full #topmenu ul li a,
.right-menu.full .mob-nav > span,
.right-menu .logo span,
.full-menu-wrap .info-wrap,
.right-menu.full .copy,
.top-menu .logo span,
.aside-menu.static #topmenu .f-right .copy {
    color: <?php echo esc_html(cs_get_option( 'menu_font_color')) ?>;
}
.right-menu.full .mob-nav.active .line,
.right-menu.full .mob-nav .line,
.aside-menu .aside-nav .aside-nav-line {
    background-color: <?php echo esc_html(cs_get_option( 'menu_font_color')) ?>;
}
@media only screen and (max-width: <?php echo esc_attr($max_mobile); ?>) {
    .mob-nav-close,
    #topmenu ul.menu li a {
        color: <?php echo esc_html(cs_get_option( 'menu_font_color')) ?>;
    }
    .mob-nav-close .line,
    .right-menu .mob-nav .line {
        background-color: <?php echo esc_html(cs_get_option( 'menu_font_color')) ?>;
    }
}

<?php }
if (cs_get_option( 'menu_font_color_t') && cs_get_option('menu_font_color_t') !== '#222222' ) { ?>
.header_trans-fixed #topmenu ul li a,
.header_trans-fixed #topmenu .search-icon-wrapper i,
.header_trans-fixed .mini-cart-wrapper .awa-shop-icon:before,
.header_trans-fixed #topmenu .awa-top-social .social li a,
.header_trans-fixed .header_top_bg .right-menu.full #topmenu ul li a,
.header_trans-fixed .right-menu.full .mob-nav > span,
.header_trans-fixed .right-menu .logo span,
.header_trans-fixed .full-menu-wrap .info-wrap,
.header_trans-fixed .right-menu.full .copy,
.header_trans-fixed .top-menu .logo span,
.header_trans-fixed .aside-menu.static #topmenu .f-right .copy {
    color: <?php echo esc_html(cs_get_option( 'menu_font_color_t')) ?>;
}
.header_trans-fixed .right-menu.full .mob-nav.active .line,
.header_trans-fixed .right-menu.full .mob-nav .line {
    background-color: <?php echo esc_html(cs_get_option( 'menu_font_color_t')) ?>;
}
@media only screen and (max-width: <?php echo esc_attr($max_mobile); ?>) {
    .header_trans-fixed .mob-nav-close,
    .header_trans-fixed #topmenu ul.menu li a {
        color: <?php echo esc_html(cs_get_option( 'menu_font_color_t')) ?>;
    }
    .header_trans-fixed .mob-nav-close .line,
    .header_trans-fixed .right-menu .mob-nav .line {
        background-color: <?php echo esc_html(cs_get_option( 'menu_font_color_t')) ?>;
    }
}

<?php }
if (cs_get_option( 'submenu_bg_color') && cs_get_option('submenu_bg_color') !== '#ffffff' ) { ?>
@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) {
    header:not(.aside-menu) #topmenu ul li.mega-menu:hover > ul::before,
    .right-menu.left #topmenu .sub-menu,
    .aside-menu.static #topmenu .sub-menu {
        background-color: <?php echo esc_html(cs_get_option( 'submenu_bg_color')) ?>;
    }
}

<?php }
if (cs_get_option( 'menu_bg_color_scroll') && cs_get_option('menu_bg_color_scroll') !== '#ffffff' ) { ?>
.header_trans-fixed.header_top_bg.bg-fixed-color {
    background-color: <?php echo esc_html(cs_get_option( 'menu_bg_color_scroll')) ?> !important;
}


<?php }
if (cs_get_option( 'menu_text_color_scroll') && cs_get_option('menu_text_color_scroll') !== '#222222' ) { ?>
.header_trans-fixed.header_top_bg.bg-fixed-color #topmenu ul li a,
.header_trans-fixed.header_top_bg.bg-fixed-color #topmenu .search-icon-wrapper i,
.header_trans-fixed.header_top_bg.bg-fixed-color #topmenu .mini-cart-wrapper .awa-shop-icon::before,
.header_trans-fixed.header_top_bg.bg-fixed-color #topmenu .awa-top-social .social li a,
.header_trans-fixed.header_top_bg.bg-fixed-color .header_top_bg .right-menu.full #topmenu ul li a,
.header_trans-fixed.header_top_bg.bg-fixed-color .right-menu.full .mob-nav > span,
.header_trans-fixed.header_top_bg.bg-fixed-color .right-menu .logo span,
.header_trans-fixed.header_top_bg.bg-fixed-color .full-menu-wrap .info-wrap,
.header_trans-fixed.header_top_bg.bg-fixed-color .right-menu.full .copy,
.header_trans-fixed.header_top_bg.bg-fixed-color .top-menu .logo span,
.header_trans-fixed.header_top_bg.bg-fixed-color .aside-menu.static #topmenu .f-right .copy {
    color: <?php echo esc_html(cs_get_option( 'menu_text_color_scroll')) ?>;
}
.header_trans-fixed.header_top_bg.bg-fixed-color .right-menu.full .mob-nav.active .line,
.header_trans-fixed.header_top_bg.bg-fixed-color .right-menu.full .mob-nav .line {
    background-color: <?php echo esc_html(cs_get_option( 'menu_text_color_scroll')) ?>;
}
@media only screen and (max-width: <?php echo esc_attr($max_mobile); ?>) {
    .header_trans-fixed.header_top_bg.bg-fixed-color .mob-nav-close,
    .header_trans-fixed.header_top_bg.bg-fixed-color #topmenu ul.menu li a {
        color: <?php echo esc_html(cs_get_option( 'menu_text_color_scroll')) ?>;
    }
    .header_trans-fixed.header_top_bg.bg-fixed-color .mob-nav-close .line,
    .header_trans-fixed.header_top_bg.bg-fixed-color .right-menu .mob-nav .line {
        background-color: <?php echo esc_html(cs_get_option( 'menu_text_color_scroll')) ?>;
    }
}
<?php }

if (cs_get_option( 'border_menu_bg_color') && cs_get_option('border_menu_bg_color') !== '#030e28' ) { ?>
@media only screen and (min-width: <?php echo esc_attr($min_mobile); ?>) {
    .aside-menu .aside-nav {
        background-color: <?php echo esc_html(cs_get_option( 'border_menu_bg_color')) ?>;
    }
}

<?php }?>

/* ======= FRONT COLOR 1 ======= */

<?php if (cs_get_option( 'front_color_1') && cs_get_option( 'front_color_1') !== '#222222') : ?>
.services.slider .content-slide:hover i,
.services.accordion .accordeon .title,
.info-block-wrap .content h1,
.info-block-wrap .content h2,
.info-block-wrap .content h3,
.info-block-wrap .content h4,
.info-block-wrap .content h5,
.info-block-wrap .content h6,
.info-block-wrap .content ul,
.main-header-testimonial.left_align .content-slide .user-info .name,
.main-header-testimonial.left_align .content-slide .user-info .position,
.main-header-testimonial.left_align .content-slide .description p,
.main-header-testimonial.simple_sl .content-slide .text,
.main-header-testimonial.simple_sl .content-slide .name,
.main-header-testimonial.simple_sl .content-slide .position,
.main-header-testimonial.classic_sl .content-slide .text,
.main-header-testimonial.classic_sl .content-slide .name,
.main-header-testimonial.classic_sl .content-slide .position,
.main-header-testimonial.modern .content-slide .description p,
.team-members-wrap.slider .content-slide .member-name,
.team-members-wrap .member-name,
.skill-wrapper.linear .title,
.services.classic_slider .swiper3-pagination .swiper3-pagination-total,
.services.classic_slider .swiper3-pagination span,
.services.left .content .text,
.services.accordion .accordeon a i,
.services.default .content .text,
.services.center .content .text,
.services.slider .text,
.product-tabs-wrapper .image-wrap .product-links-wrapp .awa-link,
.product-tabs-wrapper .image-wrap .product-links-wrapp .awa-add-to-cart a,
.pricing-info .title,
.pricing-item:hover .a-btn,
.pricing-item:hover .a-btn-2,
.pricing-item:hover .a-btn-3,
.pricing-item:hover .a-btn-4,
.urban_slider .slick-current .pagination-title,
.fragment-wrapper .fragment-block .fragment-text .wrap-frag-text,
.insta-wrapper .info-hover,
.info-block-wrap.style-4 .video.only-button .video-content span,
.info-block-wrap.style-3 .video.only-button .video-content span,
.info-block-wrap.style-3 .images-wrapper .slick-arrow,
.info-block-wrap.style-2 .accordeon a,
.info-item-wrap .content ul,
.info-item-wrap .content h1,
.info-item-wrap .content h2,
.info-item-wrap .content h3,
.info-item-wrap .content h4,
.info-item-wrap .content h5,
.info-item-wrap .content h6,
.info-block-wrap .title,
.headings .title,
.faq-item .title-wrap .title,
.contacts-info-wrap.style7 .title,
.contacts-info-wrap.style3 .text,
.contacts-info-wrap.style4 .additional-content-wrap .text,
.contacts-info-wrap.style4 .additional-content-wrap .title,
.contacts-info-wrap .title,
.contacts-info-wrap .content-item div,
.contacts-info-wrap .content-item a,
.contacts-info-wrap.style2 .main-title,
.coming-soon-descr,
.coming-page-wrapper .title,
.form.btn-style-4 .input_protected_wrapper input,
.form.btn-style-3 .input_protected_wrapper:hover input,
.form.btn-style-2 .input_protected_wrapper input,
.call-to-action.classic .call-title,
.banner-slider-wrap.urban .pag-wrapper .swiper3-button-next,
.banner-slider-wrap.urban .pag-wrapper .swiper3-button-prev,
.banner-slider-wrap.horizontal_modern_reverse .title,
.banner-slider-wrap.horizontal_modern .title,
.banner-slider-wrap.horizontal_modern .swiper3-pagination span,
.about-section .title,
body,
a,
a:hover,
a:focus,
.text-dark,
.tg-a-btn-2 a,
.a-btn-2,
.tg-a-btn-3 a,
.a-btn-3:hover,
code,
kbd,
caption,
.wpb_text_column h1,
.wpb_text_column h2,
.wpb_text_column h3,
.wpb_text_column h4,
.wpb_text_column h5,
.wpb_text_column h6,
.error404 .hero-inner .title,
.error404 .hero-inner .a-btn-dark,
.error404 .main-wrapper.unit .vertical-align .bigtext,
.error404 .main-wrapper.unit .vertical-align a,
.error404 .main-wrapper.unit .vertical-align .a-btn,
.post-little-banner .page-title-blog,
.post-little-banner.empty-post-list h3,
.post-little-banner.empty-post-list input[type="submit"]:hover,
.post-media .video-content .play::before,
.post.center-style .title,
.post.center-style.format-quote .info-wrap blockquote,
.post.center-style.format-post-text .info-wrap blockquote,
.post.center-style.format-gallery .flex-direction-nav .flex-prev,
.post.center-style.format-gallery .flex-direction-nav .flex-next,
.post.center-style.format-post-slider .flex-direction-nav .flex-prev,
.post.center-style.format-post-slider .flex-direction-nav .flex-next,
.post.metro-style .info-wrap .title,
.post.metro-style.format-video .video-content .play::before,
.post.metro-style.format-post-video .video-content .play::before,
.post.metro-style.format-quote .info-wrap blockquote,
.post.metro-style.format-post-text .info-wrap blockquote,
.post.metro-style.format-gallery .flex-direction-nav .flex-prev,
.post.metro-style.format-gallery .flex-direction-nav .flex-next,
.post.metro-style.format-post-slider .flex-direction-nav .flex-prev,
.post.metro-style.format-post-slider .flex-direction-nav .flex-next,
.single-post .title,
.single-post .single-content blockquote p,
.single-post .single-content .swiper-container .description,
.single-post .single-content .swiper-arrow-right,
.single-post .single-content .swiper-arrow-left,
.post-little-banner .main-top-content>*,
.main-wrapper .col-md-4 .sidebar-item h1,
.main-wrapper .col-md-4 .sidebar-item h2,
.main-wrapper .col-md-4 .sidebar-item h3,
.main-wrapper .col-md-4 .sidebar-item h4,
.main-wrapper .col-md-4 .sidebar-item h5,
.main-wrapper .col-md-4 .sidebar-item h6,
.main-wrapper .col-md-4 .sidebar-item strong,
.main-wrapper .col-md-4 .sidebar-item table,
.main-wrapper .col-md-4 .sidebar-item table th,
.main-wrapper .col-md-4 .sidebar-item table a,
.main-wrapper .col-md-4 .sidebar-item table caption,
.main-wrapper .col-md-4 .sidebar-item .awa-recent-post-widget .recent-text a,
.recent-post-single .recent-title,
.sm-wrap-post .content .title,
.pagination.cs-pager .page-numbers.next:after,
.pagination.cs-pager .page-numbers.prev:after,
.pages,
.page-numbers,
.post-nav a span,
.single-pagination>div a.content,
.post-details .date-post span,
.post-details .author span,
.post-details .link-wrap a,
.bottom-infopwrap .likes-wrap span,
.bottom-infopwrap .count,
.bottom-infopwrap .post__likes,
.user-info-wrap .post-author__title,
.single-content.no-thumb .main-top-content .title,
.post-info span.author,
.post-info span.author a,
.comments .comment-reply-title,
.comments .content .comment-reply-link:hover,
.comments .person .author,
.comments .comments-title,
.comments .comments-title span,
#contactform h3,
.comments-form h3,
.comment-form label,
.comments.main label,
.sidebar-item ul li a,
.col-md-4 .ContactWidget .contact_url,
.col-md-4 .ContactWidget div.contact_content,
.col-md-4 .ContactWidget a.fa,
.col-md-4 .awaInstagramWidget,
.sidebar-item span.product-title,
.woocommerce .single-product div.product p.price ins,
.woocommerce .awa_product_detail div.product span.price ins,
.woocommerce .single-product div.product span.price ins,
.woocommerce ul.products.default li.product .price ins,
.awa_cart.shop_table ul .cart_item ul .product-price ins,
.awa_cart.shop_table ul .cart_item ul .product-subtotal ins,
#topmenu .awa_mini_cart .product_list_widget .mini_cart_item .mini-cart-data .mini_cart_item_price ins,
.woocommerce table.shop_table .cart_item .product-total ins,
.woocommerce ul.products li.product .awa-prod-list-image .awa-add-to-cart a,
.woocommerce-page.woocommerce .woocommerce-message a.button,
.woocommerce .single-product .star-rating,
.woocommerce .awa_product_detail .star-rating,
.woocommerce .woocommerce-thankyou-order-received,
.awa_product_detail .social-list a,
.single-product .product .summary .product_title,
.awa_product_detail .product .summary .product_title,
.single-product .product .summary .variations_form.cart .variations_button span,
.single-product .product .summary .variations_form.cart .variations tbody span,
.awa_product_detail .product .summary .variations_form.cart .variations_button span,
.awa_product_detail .product .summary .variations_form.cart .variations tbody span,
.single-product .product .summary .cart .variations .label label,
.awa_product_detail .product .summary .cart .variations .label label,
.single-product .product .summary .cart .variations .value ul li label,
.awa_product_detail .product .summary .cart .variations .value ul li label,
.single-product .product .summary .product_meta,
.awa_product_detail .product .summary .product_meta,
.single-product div.product .woocommerce-tabs ul.tabs.wc-tabs li a,
.awa_product_detail div.product .woocommerce-tabs ul.tabs.wc-tabs li a,
.single-product div.product .woocommerce-tabs ul.tabs.wc-tabs li.active a,
.awa_product_detail div.product .woocommerce-tabs ul.tabs.wc-tabs li.active a,
.single-product div.product .woocommerce-tabs .woocommerce-Tabs-panel h2,
.awa_product_detail div.product .woocommerce-tabs .woocommerce-Tabs-panel h2,
.single-product .product #reviews #comments .commentlist .comment .comment-text .meta,
.awa_product_detail .product #reviews #comments .commentlist .comment .comment-text .meta,
.single-product .product #reviews #comments .commentlist .comment .comment-text .date_publish,
.awa_product_detail .product #reviews #comments .commentlist .comment .comment-text .date_publish,
.single-product .product .woocommerce-Reviews #review_form_wrapper .comment-reply-title,
.awa_product_detail .product .woocommerce-Reviews #review_form_wrapper .comment-reply-title,
.single-product .product div.related.products .related-subtitle,
.awa_product_detail .product div.related.products .related-subtitle,
.single-product div.product .up-sells h2,
.awa_product_detail div.product .up-sells h2,
.single-product .product .related.products h2.woocommerce-loop-product__title,
.awa_product_detail .product .related.products h2.woocommerce-loop-product__title,
.woocommerce ul.products li.product .awa-prod-list-image .awa-link,
.woocommerce ul.products li.product h3,
.awa-woocommerce-pagination .nav-links .nav-previous a,
.awa-woocommerce-pagination .nav-links .nav-next a,
.awa-woocommerce-pagination .nav-links a::before,
.awa-woocommerce-pagination .nav-links .nav-next a::after,
.awa_cart.shop_table .heading li,
.awa_cart.shop_table ul .cart_item ul .product-name a,
.awa_cart.shop_table ul .cart_item ul .product-name .variation dt,
.woocommerce form .form-row select,
.awa-cart-collaterals .cart_totals h2,
.woocommerce form.checkout h3,
.woocommerce form.login .form-row label,
.woocommerce form.checkout .form-row label,
.select2-container--default .select2-selection--single .select2-selection__rendered,
.woocommerce form.login .lost_password a,
.select2-drop-active,
.select2-results li.select2-highlighted,
.woocommerce table.shop_table thead .product-name,
.woocommerce table.shop_table thead .product-total,
.woocommerce table.shop_table .cart_item .product-name .variation dt,
.woocommerce table.shop_table tfoot .cart-subtotal th,
.woocommerce table.shop_table tfoot .shipping th,
.woocommerce table.shop_table .order-total th,
.woocommerce-checkout-review-order #payment div.payment_box,
.woocommerce .sidebar-item a.remove,
.woocommerce a.remove:hover,
.woocommerce ul.products li.product a h2,
.awa-best-seller-widget .seller-text a,
.awa-shop-banner .awa-shop-title,
.simple_gallery .flex-direction-nav a,
.simple_gallery .categories a:hover,
.simple_gallery .title,
.simple_gallery .info-item-wrap .name,
.parallax-window .content-parallax .title,
.parallax-window .content-parallax .category-parallax a:hover,
.parallax-window .content-parallax .info-item-wrap .item .name,
.team-members-wrap.slider .swiper3-button-prev,
.team-members-wrap.slider .swiper3-button-next,
.text-dark p,
.col-md-4 .sidebar-item li:hover>a,
.col-md-4 .widget_rss a ,
.col-md-4 .sidebar-item li,
.col-md-4 .sidebar-item h5,
.col-md-4 .sidebar-item select,
.col-md-4 .widget_rss a,
.col-md-4 .widget_rss span,
.col-md-4 .widget_rss cite,
.woocommerce .woocommerce-message a,
.woocommerce .woocommerce-info a,
.woocommerce .woocommerce-error a,
.woocommerce table.shop_table .cart_item .product-name strong,
.woocommerce table.shop_table tfoot .cart-subtotal td .woocommerce-Price-amount,
.comments .content .text h1,
.comments .content .text h2,
.comments .content .text h3,
.comments .content .text h4,
.comments .content .text h5,
.comments .content .text h6 {
    color: <?php echo esc_html(cs_get_option( 'front_color_1')) ?>;
}
#contactform textarea::-moz-placeholder,
#contactform input::-moz-placeholder,
.comments-form textarea::-moz-placeholder,
.comments-form input::-moz-placeholder,
.coming-page-wrapper .form input::-webkit-input-placeholder,
.coming-page-wrapper .form input::-moz-placeholder,
.coming-page-wrapper .form input:-ms-input-placeholder,
.coming-page-wrapper .form input:-moz-placeholder,
.woocommerce form.login .form-row input::-webkit-input-placeholder,
.woocommerce form.login .form-row textarea::-webkit-input-placeholder,
.woocommerce form.checkout .form-row input::-webkit-input-placeholder,
.woocommerce form.checkout .form-row textarea::-webkit-input-placeholder,
.woocommerce form.login .form-row input:-moz-placeholder,
.woocommerce form.login .form-row textarea:-moz-placeholder,
.woocommerce form.checkout .form-row input:-moz-placeholder,
.woocommerce form.checkout .form-row textarea:-moz-placeholder,
.woocommerce form.login .form-row input:-ms-input-placeholder,
.woocommerce form.login .form-row textarea:-ms-input-placeholder,
.woocommerce form.checkout .form-row input:-ms-input-placeholder,
.woocommerce form.checkout .form-row textarea:-ms-input-placeholder,
.woocommerce form.login .form-row input:-moz-placeholder,
.woocommerce form.login .form-row textarea:-moz-placeholder,
.woocommerce form.checkout .form-row input:-moz-placeholder,
.woocommerce form.checkout .form-row textarea:-moz-placeholder,
.woocommerce form.checkout_coupon .form-row input.input-text::-webkit-input-placeholder,
.woocommerce form.checkout_coupon .form-row input.input-text:-moz-placeholder,
.woocommerce form.checkout_coupon .form-row input.input-text:-ms-input-placeholder,
.woocommerce form.checkout_coupon .form-row input.input-text:-moz-placeholder,
.contacts-info-wrap .form input::-webkit-input-placeholder,
.contacts-info-wrap .form input::-moz-placeholder,
.contacts-info-wrap .form input:-ms-input-placeholder,
.contacts-info-wrap .form input:-moz-placeholder,
.contacts-info-wrap .form textarea::-webkit-input-placeholder,
.contacts-info-wrap .form textarea::-moz-placeholder,
.contacts-info-wrap .form textarea:-ms-input-placeholder,
.contacts-info-wrap .form textarea:-moz-placeholder,
.awa_cart.shop_table .complement-cart .coupon .input-text::-webkit-input-placeholder,
.woocommerce form .form-row select:-webkit-input-placeholder,
.woocommerce form .form-row input:-webkit-input-placeholder,
.awa_cart.shop_table .complement-cart .coupon .input-text:-moz-placeholder,
.woocommerce form .form-row select:-moz-placeholder,
.woocommerce form .form-row input:-moz-placeholder,
.awa_cart.shop_table .complement-cart .coupon .input-text:-ms-input-placeholder,
.woocommerce form .form-row select:-ms-input-placeholder,
.woocommerce form .form-row input:-ms-input-placeholder,
.awa_cart.shop_table .complement-cart .coupon .input-text:-moz-placeholder,
.woocommerce form .form-row select:-moz-placeholder,
.woocommerce form .form-row input:-moz-placeholder {
    color: <?php echo esc_html(cs_get_option( 'front_color_1')) ?>;
}

.main-header-testimonial.left_align .swiper3-pagination span,
.skill-wrapper.linear .line .active-line,
.product-tabs-wrapper .swiper3-pagination .swiper3-pagination-bullet.swiper3-pagination-bullet-active,
.urban_slider .slick-arrow:hover,
.insta-wrapper .info-hover i::before,
.insta-wrapper .info-hover i::after,
.info-block-wrap.style-3 .images-wrapper .slick-arrow:hover,
canvas,
.banner-slider-wrap.horizontal_modern_reverse .swiper3-pagination span,
.banner-slider-wrap.horizontal_modern .swiper3-pagination span:first-child::before,
.post-media .video-content .play:hover,
.post.center-style.format-gallery .flex-direction-nav .flex-prev:hover,
.post.center-style.format-gallery .flex-direction-nav .flex-next:hover,
.post.center-style.format-post-slider .flex-direction-nav .flex-prev:hover,
.post.center-style.format-post-slider .flex-direction-nav .flex-next:hover,
.post.center-style.format-link,
.post.center-style.format-post-link,
.post.metro-style.format-link .post-wrap-item,
.post.metro-style.format-post-link .post-wrap-item,
.post.metro-style.format-link .post-wrap-item,
.post.metro-style.format-post-link .post-wrap-item,
.post.metro-style.format-link .info-wrap,
.post.metro-style.format-post-link .info-wrap,
.post.metro-style.format-gallery .flex-direction-nav .flex-prev:hover,
.post.metro-style.format-gallery .flex-direction-nav .flex-next:hover,
.post.metro-style.format-post-slider .flex-direction-nav .flex-prev:hover,
.post.metro-style.format-post-slider .flex-direction-nav .flex-next:hover,
.single-post .single-content .swiper-arrow-right div::before,
.single-post .single-content .swiper-arrow-left div::before,
.post-details .single-categories a:hover,
.post-info span a,
.col-md-4 .widget_tag_cloud a,
.sidebar-item .price_slider_amount button.button,
.woocommerce ul.products li.product .awa_product_list_name .count,
.woocommerce form.login .form-row input[type="submit"]:focus,
.woocommerce form.login .form-row input[type="submit"]:visited,
.woocommerce form.login .form-row input[type="submit"]:active,
.woocommerce form.login .form-row input[type="submit"],
.woocommerce form.login .form-row input[type="checkbox"]:checked+label.checkbox:before,
.woocommerce form.checkout .form-row input[type="checkbox"]:checked+label.checkbox:before,
.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
.simple_slider .post-media .swiper3-pagination-bullet-active,
button,
html input[type=button],
input[type=reset],
input[type=submit],
.video.only-button .video-container iframe,
.video.only-button .fluid-width-video-wrapper iframe,
.black,
.highlight,
.blog .mfp-fade.mfp-bg.mfp-ready,
.archive .mfp-fade.mfp-bg.mfp-ready,
.pagination a.img,
.toggle-title:after,
.info-block-wrap.style-4 .video.only-button .video-container iframe,
.info-block-wrap.style-4 .video.only-button .fluid-width-video-wrapper iframe,
.info-block-wrap.style-3 .video.only-button .video-container iframe,
.info-block-wrap.style-3 .video.only-button .fluid-width-video-wrapper iframe,
::-moz-selection,
::selection,
::-moz-selection,
.ri-grid ul li,
.flex-control-paging li a.flex-active {
    background-color: <?php echo esc_html(cs_get_option( 'front_color_1')) ?>;
}


button,
html input[type=button],
input[type=reset],
input[type=submit],
.select2-drop-active,
.main-header-testimonial.left_align .swiper3-pagination span.swiper3-pagination-bullet-active::before,
.product-tabs-wrapper .swiper3-pagination .swiper3-pagination-bullet.swiper3-pagination-bullet-active::before,
.banner-slider-wrap.horizontal_modern_reverse .swiper3-pagination span.swiper3-pagination-bullet-active::before,
.flex-control-paging li {
    border-color: <?php echo esc_html(cs_get_option( 'front_color_1')) ?>;
}

.error404 .main-wrapper.unit .vertical-align .a-btn::after {
    border-left-color: <?php echo esc_html(cs_get_option( 'front_color_1')) ?>;
}

.error404 .main-wrapper.unit .vertical-align a {
    border-bottom-color: <?php echo esc_html(cs_get_option( 'front_color_1')) ?>;
}

.post-nav a span,
.woocommerce .sidebar-item a.remove,
.woocommerce a.remove:hover
{
    color:  <?php echo esc_html(cs_get_option( 'front_color_1')) ?> !important;
}
@media only screen and (max-width: 991px) {
    .fragment-wrapper .fragment-block .fragment-text .wrap-frag-text,
    .fragment-wrapper .fragment-block .fragment-text .wrap-frag-text .a-btn-4,
    .fragment-wrapper .fragment-block .fragment-text .wrap-frag-text .desc {
        color: <?php echo esc_html(cs_get_option( 'front_color_1')) ?>;
    }
}
<?php endif;

/* ======= FRONT COLOR 2 ======= */

if (cs_get_option( 'front_color_2') && cs_get_option( 'front_color_2') !== '#999999') : ?>
.main-header-testimonial.left_align .content-slide .user-info .position,
.main-header-testimonial.modern .content-slide .position,
.team-members-wrap .member-position,
.skill-wrapper.linear-text .text,
.skill-wrapper.circle .skill-text,
.skill-wrapper.linear .skill-value,
.skill-wrapper.linear .skill-label,
.service-list-wrapper .text,
.services.classic_slider .swiper3-pagination .swiper3-pagination-total,
.services.classic_slider .content-slide .text,
.services.accordion .accordeon .text,
.services.accordion .accordeon a,
.services.classic .text,
.product-tabs-wrapper .price del span,
.product-tabs-wrapper .price del,
.product-tabs-wrapper .price span,
.product-slider-wrapper .additional-link,
.product-slider-wrapper .prod-descr,
.pricing-info .subtitle,
.pricing-info .pricing-list p,
.pricing-info .pricing-list ul li,
.post-slider-wrapper .date,
.fragment-wrapper .fragment-block .fragment-text .wrap-frag-text .desc,
.info-block-wrap.style-2 .accordeon .text,
.info-block-wrap .content p,
.faq-item .text,
.contacts-info-wrap.style7 .text,
.contacts-info-wrap.style6 .item-wrapper .title,
.contacts-info-wrap.style5 .item-wrapper a,
.contacts-info-wrap.style5 .item-wrapper .text,
.contacts-info-wrap.style4 .additional-content-wrap .content-item div,
.contacts-info-wrap.style4 .additional-content-wrap .content-item a,
.contacts-info-wrap.style4 .additional-content-wrap,
.coming-page-wrapper .subtitle,
.call-to-action.simple .info-wrap .text a:hover,
.call-to-action.classic-bg .info-wrap .subtitle,
.call-to-action.classic-bg input:not([type="submit"]),
.call-to-action.center input:not([type="submit"]),
.banner-slider-wrap.urban .pag-wrapper .swiper3-pagination-total,
.banner-slider-wrap.urban .pag-wrapper .swiper3-pagination,
.banner-slider-wrap.horizontal_modern_reverse .sub-title,
.about-section-simple .subtitle,
.wpb_text_column p,
.error404 .hero-inner .subtitle,
.post-little-banner .count-results,
.post-little-banner.empty-post-list input:not([type="submit"]),
.post.center-style .date a,
.post.center-style.format-quote .info-wrap cite,
.post.center-style.format-post-text .info-wrap cite,
.post.metro-style .info-wrap .date a,
.post.metro-style .info-wrap .text p,
.post.metro-style .info-wrap .counters span,
.post.metro-style .info-wrap .counters .count,
.single-post p,
.main-wrapper .col-md-4 .sidebar-item a,
.main-wrapper .col-md-4 .sidebar-item li,
.main-wrapper .col-md-4 .sidebar-item p,
.main-wrapper .col-md-4 .sidebar-item.widget_tag_cloud a,
.main-wrapper .col-md-4 .sidebar-item .awa-widget-about .text,
.main-wrapper .col-md-4 .sidebar-item .awa-recent-post-widget .recent-date,
.single-pagination>div,
.post-details .date-post,
.post-details .author,
.post-details ul li,
.post-info .single-tags a,
.bottom-infopwrap .single-tags a,
.user-info-wrap .single-tags a,
.main-top-content .single-tags a,
.post-details .link-wrap .single-tags a,
.post-details .post-media .single-tags a,
.user-info-wrap .post-author__nicename,
.comments .content .text,
.comments .person .comment-date,
.widget_product_search input[type="search"],
.widget_search input[type="text"],
p.cart-empty,
.woocommerce-page.woocommerce-cart .woocommerce input.button,
.woocommerce-page.woocommerce-checkout .woocommerce input.button,
.woocommerce #respond input#submit,
.woocommerce a.button,
.woocommerce button.button,
.woocommerce input.button,
.shipping-calculator-button,
.woocommerce .quantity .qty,
.woocommerce table.shop_table tfoot td,
.woocommerce .product-total,
.woocommerce .shipped_via,
.single-product .product .summary .woocommerce-product-rating .woocommerce-review-link,
.awa_product_detail .product .summary .woocommerce-product-rating .woocommerce-review-link,
.single-product .product .summary .product_desc p,
.awa_product_detail .product .summary .product_desc p,
.single-product .product .summary .variations_form.cart .variations .value select,
.awa_product_detail .product .summary .variations_form.cart .variations .value select,
.single-product .product .summary .product_meta a,
.awa_product_detail .product .summary .product_meta a,
.single-product .product .summary .product_meta .sku_wrapper .sku,
.awa_product_detail .product .summary .product_meta .sku_wrapper .sku,
.single-product div.product .woocommerce-tabs .woocommerce-Tabs-panel p,
.awa_product_detail div.product .woocommerce-tabs .woocommerce-Tabs-panel p,
.single-product .product .woocommerce-Reviews #review_form_wrapper .comment-form-rating label,
.awa_product_detail .product .woocommerce-Reviews #review_form_wrapper .comment-form-rating label,
.single-product .product .woocommerce-Reviews #review_form_wrapper input,
.awa_product_detail .product .woocommerce-Reviews #review_form_wrapper input,
.single-product .product .woocommerce-Reviews #review_form_wrapper textarea,
.awa_product_detail .product .woocommerce-Reviews #review_form_wrapper textarea,
.awa-cart-collaterals .cart_totals .shop_table ul li,
.select2-search input,
.select2-results li,
.woocommerce table.shop_table .cart_item .product-name .variation dd p,
.woocommerce-checkout-review-order #payment .payment_methods.methods li,
.woocommerce-checkout-review-order #payment .payment_methods.methods li label,
.woocommerce-checkout-review-order #payment .payment_methods.methods li .about_paypal,
.woocommerce div.product p.stock,
.awa-sorting-products-widget .woocommerce-ordering select,
.awa-shop-banner .awa-shop-menu ul li,
.awa-shop-banner .awa-shop-menu ul li a,
.awa-shop-main-banner ul li,
.awa-shop-main-banner ul li a,
.simple_gallery .text p,
.simple_gallery .info-item-wrap .text-item,
.simple_gallery .info-item-wrap .text-item a,
.simple_slider .info-wrap .text-item a,
.simple_slider .text-wrap .text,
.simple_slider .blockquote cite,
.urban .banner-wrap .excerpt,
.urban .info-item-wrap .text-item,
.urban .info-item-wrap .text-item a,
.urban .text-wrap .text,
.urban .blockquote cite,
.tile_info .text-gallery-wrap .info-item-wrap .text-item,
.tile_info .text-gallery-wrap .info-item-wrap .text-item a,
.tile_info .text-gallery-wrap .text-wrap .text,
.tile_info .blockquote cite,
.alia .text-gallery-wrap .info-item-wrap .text-item,
.alia .text-gallery-wrap .info-item-wrap a,
.alia .text-wrap .text p,
.menio .blockquote cite,
.menio .additional-text,
.menio .text-wrap p,
.parallax-window .content-parallax .text,
.parallax-window .content-parallax .info-item-wrap .item .text-item a,
.parallax-window .content-parallax .info-item-wrap .item .text-item,
.portfolio-single-content.left_gallery .info-wrap .text,
.portfolio-single-content.left_gallery .info-item-wrap .text-item,
.portfolio-single-content.left_gallery .info-item-wrap .text-item a,
.blog.metro+.metro-load-more .metro-load-more__button,
.call-to-action.simple .info-item-wrap .call-subtitle,
.call-to-action.simple .info-wrap p,
blockquote cite,
.bottom-infopwrap .social-list a,
.user-info-wrap .post-author__social a,
.simple_slider .info-wrap .social-list a,
.urban .social-list a,
.tile_info .social-list a,
.alia .social-list a,
.menio .social-list a,
.post.metro-style.format-link .link-wrap i,
.post.metro-style.format-post-link .link-wrap i,
.contacts-info-wrap.style2 .text,
.single-post dl dd,
.comments dl dd,
.sm-wrap-post .content .excerpt,
.sm-wrap-post .content .post-date .date,
.post-info span a,
.post-info span,
.single-product .product #reviews #comments .commentlist .comment .comment-text .description,
.awa_product_detail .product #reviews #comments .commentlist .comment .comment-text .description,
.woocommerce ul.products li.product,
.woocommerce ul.products li.product .category-product a,
.select2-search:after {
    color: <?php echo esc_html(cs_get_option( 'front_color_2')) ?>;
}

.main-header-testimonial.classic_sl .swiper3-pagination span,
.services.slider .swiper3-pagination span,
.product-slider-wrapper .additional-link::before,
.filter_slider .swiper3-pagination span,
#back-to-top:hover,
.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content {
    background-color: <?php echo esc_html(cs_get_option( 'front_color_2')) ?>;
}
.widget_product_search input[type="search"],
.widget_search input[type="text"],
.single-product .product .woocommerce-Reviews #review_form_wrapper input,
.awa_product_detail .product .woocommerce-Reviews #review_form_wrapper input,
.single-product .product .woocommerce-Reviews #review_form_wrapper textarea,
.awa_product_detail .product .woocommerce-Reviews #review_form_wrapper textarea,
.comment-title {
    border-color: <?php echo esc_html(cs_get_option( 'front_color_2')) ?>;
}
<?php endif;

/* ======= FRONT COLOR 3 ======= */

if (cs_get_option( 'front_color_3') && cs_get_option( 'front_color_3') !== '#eeeeee') : ?>

.service-list-wrapper.classic .counter,
.main-header-testimonial.modern .swiper3-button-prev::before,
.main-header-testimonial.modern .swiper3-button-next::before,
.team-members-wrap.slider_modern .swiper3-button-prev,
.team-members-wrap.slider_modern .swiper3-button-next,
.showcase_slider .slide-image .arrow::before,
.urban_slider .pagination-category,
.urban_slider .pagination-title,
.main-header-testimonial.modern .bg-text,
.services.slider i,
.info-block-wrap.style-3 .bg-text,
.headings-wrap .title-bg,
.about-section .title-bg,
.about-section-simple .title-bg,
.product-tabs-wrapper .filters ul li span,
.filter_slider .portfolio-tabs-wrapper .filters ul li span,
.banner-slider-wrap.urban .socials a,
.banner-slider-wrap.urban .additional_title,
.single-product .product .related.products h2,
.awa_product_detail .product .related.products h2,
.woocommerce ul.products li.product span del,
.woocommerce ul.products li.product span del span,
.awa-best-seller-widget .seller-price del span,
.tile_info .recent-posts-wrapper .subtitle,
.menio .recent-posts-wrapper .subtitle,
.post.metro-style .info-wrap .counters i,
.bottom-infopwrap .likes-wrap .post__likes::before,
.product-slider-wrapper .bg-title,
.awa-best-seller-widget .swiper3-button-prev,
.awa-best-seller-widget .swiper3-button-next {
    color:  <?php echo esc_html(cs_get_option( 'front_color_3')) ?>;
}
.skill-wrapper.linear .line,
.post-slider-wrapper .swiper3-pagination,
.info-block-wrap.style-3 .images-wrapper .slick-arrow,
.fragment-wrapper .fragment-block .fragment-img,
.product-tabs-wrapper .swiper3-pagination .swiper3-pagination-bullet,
.product-slider-wrapper .swiper3-pagination .swiper3-pagination-bullet-active:not(:last-of-type)::after,
.product-slider-wrapper .swiper3-pagination .swiper3-pagination-bullet-active:last-of-type::before,
.headings.style3 .crumbs a:not(:last-of-type)::after,
.iframe-video.audio,
.grey,
.awa-shop-banner,
.urban .banner-wrap,
.tile_info .recent-posts-wrapper,
.menio .recent-posts-wrapper,
#back-to-top,
.pricing-item,
.blog.metro,
.archive.metro,
.post-little-banner,
.post.center-style.format-quote,
.post.center-style.format-post-text,
.user-info-wrap .post-author,
.comments .comment .content,
.post-media.iframe-video .video-container,
.post.metro-style.format-video .video-container,
.post.metro-style.format-post-video .video-container,
.services.classic_slider .swiper3-pagination span:first-child::before,
.post-info span.author,
.blog.metro+.metro-load-more {
    background-color:  <?php echo esc_html(cs_get_option( 'front_color_3')) ?>;
}
.services.classic_slider .content-slide,
.services.accordion .accordeon a,
.product-tabs-wrapper .image-wrap,
.post-slider-wrapper.slider_progress .swiper3-slide,
.tile_info .social-list a,
.coming-page-wrapper .form input:not([type="submit"]),
.coming-page-wrapper .form textarea,
.woocommerce table.shop_table tfoot,
.comments.main,
.main-wrapper .col-md-4 .sidebar-item h5,
.single-product .product .summary .variations_form.cart,
.awa_product_detail .product .summary .variations_form.cart,
.single-product .product .summary .cart .variations .value fieldset,
.awa_product_detail .product .summary .cart .variations .value fieldset,
.single-product .product .woocommerce-tabs .tabs.wc-tabs:before,
.awa_product_detail .product .woocommerce-tabs .tabs.wc-tabs:before,
.awa_cart.shop_table .heading,
.awa_cart.shop_table ul .cart_item,
.woocommerce table.shop_table tfoot,
.woocommerce table.shop_table .cart-subtotal,
.woocommerce table.shop_table .shipping,
.awa-best-seller-widget .swiper3-slide,
.alia .text-gallery-wrap .info-item-wrap .name,
#contactform textarea,
#contactform input:not([type="submit"]),
.comments-form textarea,
.comments-form input:not([type="submit"]),
.woocommerce-page.woocommerce-cart .woocommerce input.button,
.woocommerce-page.woocommerce-checkout .woocommerce input.button,
.woocommerce #respond input#submit,
.woocommerce a.button,
.woocommerce button.button,
.woocommerce input.button,
.shipping-calculator-button,
.awa_cart.shop_table .complement-cart .coupon .input-text,
.woocommerce form .form-row select,
.woocommerce form .form-row input,
.woocommerce form.checkout_coupon .form-row input.input-text,
.woocommerce form.login .form-row input,
.woocommerce form.login .form-row textarea,
.woocommerce form.checkout .form-row input,
.woocommerce form.checkout .form-row textarea,
.select2-container,
.team-members-wrap.slider .content-slide,
.services.default,
.services.slider .number,
.pricing-info .subtitle,
.faq-item:not(:last-of-type),
.post-info span,
.select2-container.select2-dropdown-open.select2-drop-above .select2-choice,
.select2-search input {
    border-color: <?php echo esc_html(cs_get_option( 'front_color_3')) ?>;
}

.skill-wrapper.circle svg circle {
    stroke: <?php echo esc_html(cs_get_option( 'front_color_3')) ?>;
}

<?php endif;

/* ======= LIGHT COLOR ======= */

if (cs_get_option( 'light_color') && cs_get_option( 'light_color') !== '#ffffff') : ?>
.pricing-item:hover .subtitle,
.pricing-item:hover .pricing-list a,
.pricing-info .pricing-list a:hover,
.video.only-button .close,
.video.only-button .video-content .play::before,
.skill-wrapper.numerical .skill-label,
.service-list-wrapper .img-wrap a,
.services.slider .content-slide:hover .title,
.services.slider .content-slide:hover .text,
.product-tabs-wrapper .image-wrap .onsale,
.product-tabs-wrapper .image-wrap .on-new,
.pricing-item:hover .pricing-list ul li,
.pricing-item:hover .title,
.post-slider-wrapper.classic_slider_progress .category,
.landing_split .content-wrap .portfolio-title,
.landing_split .content-wrap .excerpt,
.landing_split .content-wrap > a.a-btn-4,
.showcase_slider .slide-title,
.filter_slider .portfolio-tabs-wrapper a.title,
.tabs.interactive-slider a,
.urban_slider .slick-arrow:hover,
.urban_slider .slick-arrow,
.parallax-showcase-wrapper .title,
.parallax-showcase-wrapper .desc,
.physics-banner .title,
.physics-banner .subtitle,
.info-block-wrap.style-4 .video.only-button .close,
.info-block-wrap.style-4 .video.only-button .video-content .play::before,
.info-block-wrap.style-3 .video.only-button .close,
.info-block-wrap.style-3 .video.only-button .video-content .play::before,
.info-block-wrap.style-3 .images-wrapper .slick-arrow:hover,
.headings.style6 .title,
.glitch-wrapper.style-1 .title,
.glitch-wrapper.style-1 .text,
.disortion-wrap .scene-nav,
.wpcf7 .input_protected_wrapper input,
.form.btn-style-3 .input_protected_wrapper input,
.form.btn-style-2 .input_protected_wrapper:hover input,
.form .input_protected_wrapper input,
.call-to-action.default .title,
.call-to-action.default .subtitle,
.banner-slider-wrap .title,
.banner-slider-wrap.urban .subtitle,
.banner-slider-wrap.urban .text,
.banner-slider-wrap .btn-scroll-down,
.banner-slider-wrap .swiper3-pagination span,
.top-banner .socials a,
.main-album-anim-wrap,
.main-album-anim-wrap .content--layout-1 .album-text-wrap .content__subtitle,
.main-album-anim-wrap .content--layout-1 .album-text-wrap .content__title,
.main-album-anim-wrap .content__title,
.main-album-anim-wrap .content__desc,
.main-album-anim-wrap .content__link.a-btn-4,
.top-banner .title,
.top-banner .sub-title,
.top-banner.classic .title,
.text-light a,
.text-light p,
.mb_YTPPlaypause:before,
.text-light,
.highlight,
.tg-a-btn a,
.a-btn,
.tg-a-btn a:hover,
.a-btn:hover,
.tg-a-btn-2 a:hover,
.a-btn-2:hover,
.tg-a-btn-3 a,
.a-btn-3,
mark,
ins,
button,
html input[type=button],
input[type=reset],
input[type=submit],
.AwaInstagramWidget h3.insta-logo,
.AwaInstagramWidget .instagram-text,
.AwaInstagramWidget .instagram-text a,
.AwaInstagramWidget .instagram-text a:focus,
.AwaInstagramWidget .instagram-text a:active,
.AwaInstagramWidget .instagram-text a:hover,
.ContactWidget h3,
.ContactWidget div.contact_content,
.ContactWidget .contact_url,
.ContactWidget a.fa,
.post-little-banner.empty-post-list input[type="submit"],
.post-media .video-content .play:hover::before,
.post-media .close,
.post.center-style.format-gallery .flex-direction-nav .flex-prev:hover,
.post.center-style.format-gallery .flex-direction-nav .flex-next:hover,
.post.center-style.format-post-slider .flex-direction-nav .flex-prev:hover,
.post.center-style.format-post-slider .flex-direction-nav .flex-next:hover,
.post.center-style.format-link .link-wrap a,
.post.center-style.format-post-link .link-wrap a,
.post.metro-style .info-wrap .category a,
.post.metro-style.format-link .link-wrap a,
.post.metro-style.format-post-link .link-wrap a,
.post.metro-style.format-gallery .flex-direction-nav .flex-prev:hover,
.post.metro-style.format-gallery .flex-direction-nav .flex-next:hover,
.post.metro-style.format-post-slider .flex-direction-nav .flex-prev:hover,
.post.metro-style.format-post-slider .flex-direction-nav .flex-next:hover,
.post-content h5,
.post-content .date,
.post-wrap-item.text .post-content i,
.post-wrap-item.text .post-content blockquote,
.img-slider .flex-next,
.img-slider .flex-prev,
.post-details .single-categories a,
.post-info .single-tags a:hover,
.bottom-infopwrap .single-tags a:hover,
.user-info-wrap .single-tags a:hover,
.main-top-content .single-tags a:hover,
.post-details .link-wrap .single-tags a:hover,
.post-details .post-media .single-tags a:hover,
#contactform #submit,
.comments-form #submit,
.col-md-4 .widget_tag_cloud a,
.sidebar-item .price_slider_amount button.button,
.button.wc-backward,
.woocommerce div.product form.cart .button,
.woocommerce-page.woocommerce-cart .woocommerce input.button:hover,
.woocommerce-page.woocommerce-checkout .woocommerce input.button:hover,
.woocommerce #respond input#submit:hover,
.woocommerce a.button:hover,
.woocommerce button.button:hover,
.woocommerce input.button:hover,
.shipping-calculator-button:hover,
.woocommerce-page.woocommerce-cart a.button,
.woocommerce-page.woocommerce-checkout a.button,
.woocommerce-page.woocommerce a.button,
.woocommerce-page.woocommerce button.button.alt,
.woocommerce button.button.alt,
.woocommerce-page.woocommerce-cart a.button:hover,
.woocommerce-page.woocommerce-checkout a.button:hover,
.woocommerce-page.woocommerce a.button:hover,
.woocommerce-page.woocommerce button.button.alt:hover,
.woocommerce button.button.alt:hover,
.woocommerce .awa_images span.onsale,
.woocommerce ul.products li.product .awa-prod-list-image .onsale,
.woocommerce .woocommerce-error,
.product-gallery-wrap .on-new,
.single-product .product .summary .cart .group_table td.label,
.awa_product_detail .product .summary .cart .group_table td.label,
.single-product .product .summary .cart .button:hover,
.awa_product_detail .product .summary .cart .button:hover,
.single-product .product .woocommerce-Reviews #review_form_wrapper .comment-form .form-submit input#submit,
.awa_product_detail .product .woocommerce-Reviews #review_form_wrapper .comment-form .form-submit input#submit,
.input_shop_wrapper:hover,
.woocommerce ul.products li.product .awa_product_list_name .count,
.woocommerce form.login .form-row input[type="submit"]:focus,
.woocommerce form.login .form-row input[type="submit"]:visited,
.woocommerce form.login .form-row input[type="submit"]:active,
.woocommerce form.login .form-row input[type="submit"],
.shop-list-page .on-new,
.woocommerce ul.products li.product span.on-new,
.alia .banner-wrap .title,
.menio .banner-wrap .title,
.menio .banner-wrap .social-list li a,
.post.metro-style.format-quote i.fa-quote-right,
.post.metro-style.format-post-text i.fa-quote-right,
.sidebar-item ul li
{
    color: <?php echo esc_html(cs_get_option( 'light_color')) ?>;
}

::-moz-selection,
::selection,
::-moz-selection {
    color: <?php echo esc_html(cs_get_option( 'light_color')) ?>;
}

.video.only-button .video-content .play:hover,
.main-header-testimonial.simple_sl .content-slide,
.main-header-testimonial.simple_sl .swiper3-pagination span.swiper3-pagination-bullet-active,
.main-header-testimonial.simple_sl .swiper3-pagination span,
.main-header-testimonial.classic_sl .content-slide .info-wrap,
.team-members-wrap.slider_modern .team-member .social,
.team-members-wrap.inline_text .team-member .social,
.service-list-wrapper .content-wrap,
.services.accordion .accordeon-wrap,
.services.default,
.services.slider .content-slide,
.services.slider .number,
.product-tabs-wrapper .image-wrap::after,
.pricing-item:hover .a-btn,
.pricing-item:hover .a-btn-2,
.pricing-item:hover .a-btn-3,
.pricing-item:hover .a-btn-4,
.post-slider-wrapper.slider_progress .content-wrap,
.split-wrapper .vertical::before,
.split-wrapper .vertical::after,
.split-wrapper .horizontal::before,
.split-wrapper .horizontal::after,
.urban_slider .slick-arrow,
.info-block-wrap.style-3 .video.only-button .video-content .play:hover,
.contacts-info-wrap.style7,
.contacts-info-wrap.style4 .additional-content-wrap,
.form.btn-style-3 .input_protected_wrapper:hover input,
.form.btn-style-2 .input_protected_wrapper input,
.banner-slider-wrap.vertical_custom_elements .swiper3-pagination .swiper3-pagination-bullet-active i::before,
.banner-slider-wrap.urban .pag-wrapper,
.banner-slider-wrap.urban .additional_wrap,
.preloader-svg,
.tg-a-btn-2 a,
.a-btn-2,
.tg-a-btn-3 a,
.a-btn-3:hover,
.main-wrapper.footer-parallax,
.post-little-banner.empty-post-list input[type="submit"]:hover,
.post-media .video-content .play,
.post.center-style .info-wrap,
.post.center-style.format-gallery .flex-direction-nav .flex-prev,
.post.center-style.format-gallery .flex-direction-nav .flex-next,
.post.center-style.format-post-slider .flex-direction-nav .flex-prev,
.post.center-style.format-post-slider .flex-direction-nav .flex-next,
.post.metro-style .post-wrap-item,
.post.metro-style .info-wrap,
.post.metro-style.format-video .video-content .play,
.post.metro-style.format-post-video .video-content .play,
.post.metro-style.format-gallery .flex-direction-nav .flex-prev,
.post.metro-style.format-gallery .flex-direction-nav .flex-next,
.post.metro-style.format-post-slider .flex-direction-nav .flex-prev,
.post.metro-style.format-post-slider .flex-direction-nav .flex-next,
.single-post .single-content .swiper-container .description,
.single-post .single-content .swiper-arrow-right,
.single-post .single-content .swiper-arrow-left,
.pages,
.page-numbers,
.post-banner,
.simple_gallery .flex-direction-nav a,
.parallax-window .content-parallax,
.white,
body,
.flex-control-paging li a,
.team-members-wrap.inline .team-member .member-info-wrap,
.banner-slider-wrap.vertical .swiper3-pagination .swiper3-pagination-bullet-active i::before,
.banner-slider-wrap .swiper3-pagination span:first-child::before
{
    background-color: <?php echo esc_html(cs_get_option( 'light_color')) ?>;
}
.woocommerce ul.products li.product .awa-prod-list-image::after,
.banner-slider-wrap .swiper3-pagination span:first-child::before,
.banner-slider-wrap.vertical .swiper3-pagination .swiper3-pagination-bullet-active i::before {
    background-color: <?php echo esc_html(cs_get_option( 'light_color')) ?>;
}
.main-header-testimonial.left_align .swiper3-pagination span {
    background-color: <?php echo esc_html(cs_get_option( 'light_color')) ?>!important;
}
.main-header-testimonial.simple_sl .content-slide::before,
.main-header-testimonial.classic_sl .content-slide .info-wrap::before,
.services.slider .content-slide::before,
.flex-control-paging li a {
    border-color: <?php echo esc_html(cs_get_option( 'light_color')) ?>;
}
.main-header-testimonial.left_align .swiper3-pagination span.swiper3-pagination-bullet-active::before {
    border-color: <?php echo esc_html(cs_get_option( 'light_color')) ?>!important;
}
.services.default:hover .content .title,
.services.default:hover .content .text,
.services.default:hover .content i,
.button.wc-backward,
.single-product .product .summary .cart .button:hover,
.awa_product_detail .product .summary .cart .button:hover,
.woocommerce-page.woocommerce .sidebar-item a.button {
    color: <?php echo esc_html(cs_get_option( 'light_color')) ?> !important;
}

@media only screen and (max-width: 768px) {
    .post-slider-wrapper.slider_progress .date,
    .post-slider-wrapper.slider_progress .title {
        color: <?php echo esc_html(cs_get_option( 'light_color')) ?>;
    }
}
@media only screen and (min-width: 1200px) {
    .portfolio-single-content.left_gallery .single-pagination.left_gallery.change-color a.content
    {
        color: <?php echo esc_html(cs_get_option( 'light_color')) ?>;
    }
}
<?php endif;


/* ======= FIRST BASE COLOR ======= */

if (cs_get_option( 'base_color_1') && cs_get_option( 'base_color_1') !== '#f54ea2') : ?>
.headings .subtitle,
.video.only-button .video-content .play:hover::before,
.main-header-testimonial.modern .content-slide .description p::before,
.team-members-wrap.slider_modern .team-member .social a:hover,
.team-members-wrap.inline-text .team-member .social a:hover,
.team-members-wrap.slider .swiper3-button-prev:hover,
.team-members-wrap.slider .swiper3-button-next:hover,
.team-members-wrap.slider .content-slide .social .fa:hover,
.team-members-wrap.inline .team-member .social .fa:hover,
.skill-wrapper.circle #cont::after,
.skill-wrapper.linear .subtitle,
.service-list-wrapper .counter,
.services.accordion .accordeon a:hover,
.services.accordion .accordeon a:hover .title,
.services i,
.services.slider .number,
.product-tabs-wrapper .price ins span,
.product-tabs-wrapper .filters ul li:hover,
.product-tabs-wrapper .filters ul li:active,
.product-tabs-wrapper .filters ul li:hover span,
.product-tabs-wrapper .filters ul li:active span,
.product-slider-wrapper .swiper3-pagination .swiper3-pagination-bullet-active i,
.product-slider-wrapper .socials a:hover,
.product-slider-wrapper .additional-link:hover,
.product-slider-wrapper .additional-email:hover,
.pricing-info .pricing-list ul li::before,
.pricing-info .pricing-list a,
.pricing-info .price,
.post-slider-wrapper.category,
.filter_slider .portfolio-tabs-wrapper .filters ul li:hover,
.filter_slider .portfolio-tabs-wrapper .filters ul li.active,
.filter_slider .portfolio-tabs-wrapper .filters ul li:hover span,
.filter_slider .portfolio-tabs-wrapper .filters ul li.active span,
.urban_slider .slick-current .pagination-category,
.info-block-wrap.style-4 .video.only-button .video-content .play:hover::before,
.info-block-wrap.style-4 .content a,
.info-block-wrap.style-3 .video.only-button .video-content .play:hover::before,
.info-block-wrap.style-3 .content a,
.info-block-wrap .content ul li::before,
.info-block-wrap .subtitle,
.headings.style5 .typed,
.headings.style5 .typed-cursor,
.heading.subtitle,
.headings.style3 .crumbs a:hover,
.headings.style3 .crumbs a:last-of-type,
.faq-item .number,
.contacts-info-wrap.style5 .item-wrapper a:hover,
.contacts-info-wrap.style6 .item-wrapper a:hover,
.contacts-info-wrap.style3 .title-main,
.contacts-info-wrap.style4 .additional-content-wrap .content-item a:hover,
.contacts-info-wrap .content-item a:hover,
.coming-soon .count,
.coming-page-wrapper .form input:not([type="submit"]),
.coming-page-wrapper .form textarea,
.call-to-action.simple .info-wrap .text a,
.banner-slider-wrap.urban .pag-wrapper .swiper3-pagination-current,
.banner-slider-wrap.urban .socials a:hover,
.banner-slider-wrap .btn-scroll-down:hover,
.banner-slider-wrap.horizontal_modern .sub-title,
.about-section .subtitle,
#footer .awa-widget-social-link a:hover,
#footer .footer-socials a:hover,
.error404 .hero-inner .bigtext,
.error404 .hero-inner .search .input-group::before,
.post-little-banner .page-title-blog span,
.post.center-style .category,
.post.center-style .category a,
.single-post .single-content blockquote p::before,
.main-wrapper .col-md-4 .sidebar-item li:hover>a:not(.rsswidget),
.main-wrapper .col-md-4 .sidebar-item a:hover,
.main-wrapper .col-md-4 .sidebar-item.widget_tag_cloud a:hover,
.page-numbers:hover,
.page-numbers:focus,
.post-nav .pages,
.post-nav .current,
.pager-pagination .pages,
.pager-pagination .current,
.single-pagination>div.pag-prev:hover::before,
.single-pagination>div.pag-next:hover::after,
.post-details .link-wrap i,
.bottom-infopwrap .likes-wrap .post__likes--liked::before,
.bottom-infopwrap .social-list a:hover,
.user-info-wrap .post-author__social a:hover,
.comments .person .author:hover,
#contactform textarea,
#contactform input:not([type="submit"]),
.comments-form textarea,
.comments-form input:not([type="submit"]),
.widget_product_search form::after,
.widget_search form div::after,
.woocommerce .awa_product_detail div.product p.price,
.woocommerce .awa_product_detail div.product p.price ins,
.woocommerce .product-name a,
.awa_product_detail .social-list a:hover,
.single-product .product .summary .product_meta a:hover,
.awa_product_detail .product .summary .product_meta a:hover,
.awa-woocommerce-pagination .nav-links .nav-previous a:hover::before,
.awa-woocommerce-pagination .nav-links .nav-previous a:hover::after,
.awa-woocommerce-pagination .nav-links .nav-next a:hover::before,
.awa-woocommerce-pagination .nav-links .nav-next a:hover::after,
.awa_cart.shop_table ul .cart_item ul .product-price,
.awa_cart.shop_table ul .cart_item ul .product-subtotal,
.awa_cart.shop_table .complement-cart .coupon .input-text,
.woocommerce form .form-row select,
.woocommerce form .form-row input,
.awa-cart-collaterals .cart_totals .shop_table ul li span,
.woocommerce form.checkout_coupon .form-row input.input-text,
.woocommerce form.login .form-row input,
.woocommerce form.login .form-row textarea,
.woocommerce form.checkout .form-row input,
.woocommerce form.checkout .form-row textarea,
.select2-container,
.woocommerce table.shop_table .order-total .woocommerce-Price-amount,
.woocommerce ul.products li.product span,
.awa-best-seller-widget .swiper3-button-prev:hover,
.awa-best-seller-widget .swiper3-button-next:hover,
.awa-best-seller-widget .seller-price span,
.awa-sorting-products-widget .woocommerce-ordering::after,
.awa-shop-banner .awa-shop-menu ul li a:hover,
.awa-shop-main-banner ul li a:hover,
.simple_gallery .categories a,
.simple_slider .info-wrap a:hover,
.simple_slider .info-wrap .social-list a:hover,
.simple_slider .blockquote::before,
.urban .info-item-wrap a:hover,
.urban .blockquote::before,
.urban .social-list a:hover,
.tile_info .text-gallery-wrap .info-item-wrap a:hover,
.tile_info .blockquote::before,
.tile_info .social-list a:hover,
.alia .text-gallery-wrap .info-item-wrap a:hover,
.alia .social-list a:hover,
.menio .banner-wrap .social-list li a:hover,
.menio .social-list a:hover,
.tg-item .tg-item-inner .main-color,
.parallax-window .content-parallax .category-parallax a,
.parallax-window .content-parallax .social-list>li a:hover,
.parallax-window .content-parallax .info-item-wrap .item .text-item a:hover,
.portfolio-single-content.left_gallery .info-item-wrap .text-item a:hover,
.portfolio-single-content.left_gallery .social-list a:hover,
.contacts-info-wrap .form input:not([type="submit"]),
.contacts-info-wrap .form textarea {
    color: <?php echo esc_html(cs_get_option( 'base_color_1')) ?>;
}

.headings .title i {
    color: <?php echo esc_html(cs_get_option( 'base_color_1')) ?> !important;
}

.video.only-button .video-content .play,
.main-header-testimonial.classic_sl .swiper3-pagination span.swiper3-pagination-bullet-active,
.services.default:hover,
.services.slider .swiper3-pagination span.swiper3-pagination-bullet-active,
.services.slider .content-slide:hover,
.pricing-item:hover,
.post-slider-wrapper .swiper3-pagination-progressbar,
.post-slider-wrapper.classic_slider_progress .category,
.filter_slider .swiper3-pagination span.swiper3-pagination-bullet-active,
.info-block-wrap.style-4 .video.only-button .video-content .play,
.info-block-wrap.style-3 .video.only-button .video-content .play,
.wpcf7 .input_protected_wrapper input,
.form.btn-style-3 .input_protected_wrapper input,
.form.btn-style-2 .input_protected_wrapper:hover input,
.form .input_protected_wrapper input,
.call-to-action.default .text-left-wrap,
.banner-slider-wrap.horizontal .style2,
.tg-a-btn a,
.a-btn,
.tg-a-btn-2 a:hover,
.a-btn-2:hover,
.tg-a-btn-3 a,
.a-btn-3,
.post-little-banner.empty-post-list input[type="submit"],
.post.metro-style .info-wrap .category a,
.post-details .single-categories a,
.post-info .single-tags a:hover,
.bottom-infopwrap .single-tags a:hover,
.user-info-wrap .single-tags a:hover,
.main-top-content .single-tags a:hover,
.post-details .link-wrap .single-tags a:hover,
.post-details .post-media .single-tags a:hover,
#contactform #submit,
.comments-form #submit,
.woocommerce div.product form.cart .button,
.woocommerce-page.woocommerce-cart .woocommerce input.button:hover,
.woocommerce-page.woocommerce-checkout .woocommerce input.button:hover,
.woocommerce #respond input#submit:hover,
.woocommerce a.button:hover,
.woocommerce button.button:hover,
.woocommerce input.button:hover,
.shipping-calculator-button:hover,
.woocommerce-page.woocommerce-cart a.button,
.woocommerce-page.woocommerce-checkout a.button,
.woocommerce-page.woocommerce a.button,
.woocommerce-page.woocommerce button.button.alt,
.woocommerce button.button.alt,
.woocommerce-page.woocommerce-cart a.button:hover,
.woocommerce-page.woocommerce-checkout a.button:hover,
.woocommerce-page.woocommerce a.button:hover,
.woocommerce-page.woocommerce button.button.alt:hover,
.woocommerce button.button.alt:hover,
.woocommerce .woocommerce-error,
.single-product .product .summary .cart .variations .value ul li input:checked+label:before,
.awa_product_detail .product .summary .cart .variations .value ul li input:checked+label:before,
.single-product .product .woocommerce-Reviews #review_form_wrapper .comment-form .form-submit input#submit,
.awa_product_detail .product .woocommerce-Reviews #review_form_wrapper .comment-form .form-submit input#submit,
.urban .banner-wrap .title::after {
    background-color: <?php echo esc_html(cs_get_option( 'base_color_1')) ?>;
}

.video.only-button .video-content .play,
.services.default:hover,
.services.slider .content-slide:hover::before,
.info-block-wrap.style-4 .video.only-button .video-content .play,
.info-block-wrap.style-3 .video.only-button .video-content .play,
.woocommerce form .form-row.woocommerce-validated .select2-container,
.woocommerce form .form-row.woocommerce-validated input.input-text,
.woocommerce form .form-row.woocommerce-validated select,
.coming-page-wrapper .form input:not([type="submit"]):focus,
.coming-page-wrapper .form textarea:focus,
.call-to-action.center input:not([type="submit"]):focus,
.call-to-action.center input:not([type="submit"]),
.post-little-banner.empty-post-list input:not([type="submit"]),
#contactform textarea:focus,
#contactform input:not([type="submit"]):focus,
.comments-form textarea:focus,
.comments-form input:not([type="submit"]):focus,
.awa_cart.shop_table .complement-cart .coupon .input-text:focus,
.woocommerce form.login .form-row input:focus,
.woocommerce form.login .form-row textarea:focus,
.woocommerce form.checkout .form-row input:focus,
.woocommerce form.checkout .form-row textarea:focus,
.woocommerce form.checkout_coupon .form-row input.input-text:focus,
.contacts-info-wrap .form input:not([type="submit"]):focus,
.contacts-info-wrap .form textarea:focus {
    border-color: <?php echo esc_html(cs_get_option( 'base_color_1')) ?>;
}
.skill-wrapper.circle svg #bar {
    stroke: <?php echo esc_html(cs_get_option( 'base_color_1')) ?>;
}



<?php endif;


/* ======= SECOND BASE COLOR ======= */

if (cs_get_option( 'base_color_2') && cs_get_option( 'base_color_2') !== '#ffdd65') : ?>
.main-header-testimonial.simple_sl .stars,
.main-header-testimonial.classic_sl .stars,
.skill-wrapper.numerical .skill-value,
.skill-wrapper.numerical .symbol,
.pricing-item:hover .pricing-list ul li::before,
.pricing-item:hover .price {
    color: <?php echo esc_html(cs_get_option( 'base_color_2')) ?>;
}

.team-members-wrap.inline .team-member .member-name::before,
.services.default::before,
.services.slider .content-slide::after,
.product-tabs-wrapper .image-wrap .product-links-wrapp .awa-add-to-cart a::before,
.tabs.interactive-slider a::before,
.info-block-wrap.style-2 .accordeon a::before,
.info-block-wrap.style-2 .image-wrap .image-container::before,
.info-block-wrap .title::before,
.wpcf7 .input_protected_wrapper::before,
.form.btn-style-4 .input_protected_wrapper::before,
.form .input_protected_wrapper::before,
.tg-a-btn a::before,
.a-btn::before,
.tg-a-btn-4 a::before,
.a-btn-4::before,
.comments .content .comment-reply-link::before,
.input_post_wrapper::before,
.woocommerce-page.woocommerce-cart a.button::before,
.woocommerce-page.woocommerce-checkout a.button::before,
.woocommerce-page.woocommerce a.button::before,
.woocommerce-page.woocommerce button.button.alt::before,
.woocommerce button.button.alt::before,
.woocommerce ul.products li.product .awa-prod-list-image .awa-add-to-cart a::before,
.woocommerce-page.woocommerce .woocommerce-message a.button::before,
.single-product div.product .woocommerce-tabs ul.tabs.wc-tabs li.active a::before,
.awa_product_detail div.product .woocommerce-tabs ul.tabs.wc-tabs li.active a::before,
.input_shop_wrapper::before {
    background-color: <?php echo esc_html(cs_get_option( 'base_color_2')) ?>;
}
.error404 .hero-inner .bigtext,
.coming-soon .count {
    text-shadow: -10px 0px 0px <?php echo esc_html(cs_get_option( 'base_color_2')) ?>;
}
@media (max-width: 480px) {
    .coming-soon-descr .count {
        text-shadow: -5px 0px 0px<?php echo esc_html(cs_get_option( 'base_color_2')) ?>;
    }
}
<?php endif;


/* ======= FOOTER BACKGROUND COLOR ======= */

if (cs_get_option( 'footer_bg_color') && cs_get_option( 'footer_bg_color') !== '#222222') : ?>
#footer {
    background-color: <?php echo esc_html(cs_get_option( 'footer_bg_color')) ?>;
}

<?php endif;


/* ======= FOOTER TEXT COLOR ======= */

if (cs_get_option( 'footer_text_color') && cs_get_option( 'footer_text_color') !== '#ffffff') : ?>
#footer .sidebar-item #wp-calendar caption,
#footer .widget_calendar th,
#footer .sidebar-item.widget_calendar table a,
#footer .sidebar-item[class*='widget_'] ul li>a:hover,
#footer .sidebar-item[class*='widget_'] h5,
#footer .sidebar-item[class*='widget_'] a:hover,
#footer .widget_product_search form::after,
#footer .widget_search form div::after,
#footer .awa-widget-social-link a,
#footer .copy_content,
#footer.fix-bottom .copyright,
#footer .footer-socials a,
#footer .copyright a,
#footer .social-links a,
#footer .sidebar-item h5,
#footer .widget_text h5,
#footer .widget_text .wpcf7-response-output,
#footer .sidebar-item[class*='widget_'] a,
#footer .sidebar-item[class*='widget_'] label,
#footer .sidebar-item[class*='widget_'] p,
#footer .sidebar-item[class*='widget_'] strong,
#footer .sidebar-item[class*='widget_'] span,
#footer .sidebar-item[class*='widget_'] caption,
#footer .awa-recent-post-widget a,
#footer .awa-recent-post-widget .recent-date,
#footer .copyright,
#footer .sidebar-item[class*='widget_'] select,
#footer.fix-bottom .footer-socials a,
#footer .AwaInstagramWidget .instagram-text {
    color: <?php echo esc_html(cs_get_option( 'footer_text_color')) ?>;
}
#footer .widget_text form input:not([type="submit"]),
#footer .widget_text form input::-webkit-input-placeholder,
#footer .widget_text form input::-moz-placeholder,
#footer .widget_text form input:-ms-input-placeholder,
#footer .widget_text form input:-moz-placeholder,
#footer .widget_product_search form::after:hover,
#footer .widget_search form div::after:hover
{
    color: <?php echo esc_html(cs_get_option( 'footer_text_color')) ?>;
}
#footer .widget_product_search form::after:hover,
#footer .widget_search form div::after:hover {
    background-color: <?php echo esc_html(cs_get_option( 'footer_text_color')) ?>;
}
<?php endif;


}





//TYPOGRAPHY

$options = apply_filters( 'cs_get_option', get_option( CS_OPTION ) );

function get_str_by_number($str){
    $number = preg_replace("/[0-9|\.]/", '', $str);
    return $number;
}

foreach ($options as $key => $item) {
    if (is_array($item)) {
        if (!empty($item['variant']) && $item['variant'] == 'regular') {
            $item['variant'] = 'normal';
        }
    }
    $options[$key] = $item;
}

function calculateFontWeight( $fontWeight ) {
    $fontWeightValue = '';
    $fontStyleValue = '';

    switch( $fontWeight ) {
        case '100':
            $fontWeightValue = '100';
            break;
        case '100italic':
            $fontWeightValue = '100';
            $fontStyleValue = 'italic';
            break;
        case '300':
            $fontWeightValue = '300';
            break;
        case '300italic':
            $fontWeightValue = '300';
            $fontStyleValue = 'italic';
            break;
        case '500':
            $fontWeightValue = '500';
            break;
        case '500italic':
            $fontWeightValue = '500';
            $fontStyleValue = 'italic';
            break;
        case '700':
            $fontWeightValue = '700';
            break;
        case '700italic':
            $fontWeightValue = '700';
            $fontStyleValue = 'italic';
            break;
        case '900':
            $fontWeightValue = '900';
            break;
        case '900italic':
            $fontWeightValue = '900';
            $fontStyleValue = 'italic';
            break;
        case 'italic':
            $fontStyleValue = 'italic';
            break;
    }

    return array('weight' => $fontWeightValue, 'style' => $fontStyleValue);
}

$all_button_font = $options['all_button_font_family']; ?>

.a-btn, .a-btn-2, .a-btn-3, .a-btn-4,
.btn-style-1 input[type="submit"],
.btn-style-2 input[type="submit"],
.btn-style-3 input[type="submit"],
.btn-style-4 input[type="submit"] {
<?php
if(!empty($all_button_font['family'])){
	echo "font-family: \"{$all_button_font['family']}\" !important;";
}

$variant = calculateFontWeight( $all_button_font['variant'] );
if(!empty($variant['style'])) : ?> font-style: <?php echo esc_html( $variant['style']); ?> !important;
<?php endif;
if(!empty($variant['weight'])) : ?> font-weight: <?php echo esc_html( $variant['weight']); ?> !important;
<?php endif;

$button_font_style = get_str_by_number($all_button_font['variant']);
if(!empty($button_font_style) && !empty($all_button_font['family'])){
	echo "font-style:{$button_font_style} !important;";
}

$all_button_font_size = get_number_str($options['all_button_font_size']);
if(!empty($all_button_font_size)){
	echo "font-size: {$all_button_font_size}px !important;";
}

$all_button_line_height = get_number_str($options['all_button_line_height']);
if(!empty($all_button_line_height)){
   echo "line-height:{$all_button_line_height}px !important;";
}
if(!empty($options['all_button_letter_spacing'])){
	echo "letter-spacing:{$options['all_button_letter_spacing']} !important;";
} ?>
}

<?php $all_links_font= $options['all_links_font_family']; ?>
a {
<?php if(!empty($all_links_font['family'])){
	echo "font-family: \"{$all_links_font['family']}\" !important;";
}
$variant = calculateFontWeight( $all_links_font['variant'] );
if(!empty($all_links_font['family']) && !empty($variant['style'])) : ?> font-style: <?php echo esc_html( $variant['style']); ?> !important;
<?php endif;
if(!empty($variant['weight'])) : ?> font-weight: <?php echo esc_html( $variant['weight']); ?> !important;
<?php endif;

$links_font_family = get_str_by_number($all_links_font['variant']);
if(!empty($links_font_family) && !empty($all_links_font['family'])) {
	echo "font-style:{$links_font_family} !important;";
}

$all_links_font_size = get_number_str($options['all_links_font_size']);
if(!empty($all_links_font_size)){
	echo "font-size: {$all_links_font_size}px !important;" ;
}

$all_links_line_height = get_number_str($options['all_links_line_height']);
if(!empty($all_links_line_height)){
	echo "line-height:{$all_links_line_height}px !important;";
}

$all_links_letter_spacing = get_number_str($options['all_links_letter_spacing']);
if(!empty($all_links_letter_spacing)){
	echo "letter-spacing:{$all_links_letter_spacing} !important;";
} ?>
}

/*FOOTER*/
<?php function get_number_str($str){
    $number = preg_replace("/[^0-9|\.]/", '', $str);
    return $number;
}


/* FOR TITLE H1 - H6 */
if ( cs_get_option('heading') ) {
    foreach (cs_get_option('heading') as $title) {
        $font_family = $title['heading_family'];
        echo esc_attr($title['heading_tag']); ?> ,
<?php echo esc_attr($title['heading_tag']); ?> a {
                                               <?php if($font_family['family']){
												   echo "font-family: {$font_family['family']} !important;";
											   }
											   $one_title_size = get_number_str($title['heading_size']);
											   if($one_title_size){
												   echo "font-size: {$one_title_size}px !important;\n line-height: normal;";
											   }?>
                                               }

<?php }
} ?>

#topmenu ul.menu > li > a {
<?php if ( cs_get_option('menu_item_family') ) {

	$font_family = cs_get_option('menu_item_family');
	if(!empty($font_family['family'])){ ?> font-family: "<?php echo esc_html( $font_family['family'] ); ?>", sans-serif;
<?php }

$variant = calculateFontWeight( $font_family['variant'] );
if(!empty($variant['style'])) : ?> font-style: <?php echo esc_html( $variant['style']); ?> !important;
<?php endif;
if(!empty($variant['weight'])) : ?> font-weight: <?php echo esc_html( $variant['weight']); ?> !important;
<?php endif;
}
if ( cs_get_option('menu_item_size') ) {
$menu_item_size = get_number_str(cs_get_option('menu_item_size'));  ?> font-size: <?php echo esc_html( $menu_item_size ); ?>px!important;
<?php }
if ( cs_get_option('menu_line_height') ) {
	$menu_line_height = get_number_str(cs_get_option('menu_line_height'));  ?> line-height: <?php echo esc_html( $menu_line_height ); ?>px!important;
<?php } ?>
}

#topmenu ul ul li a {
<?php if ( cs_get_option('submenu_item_family') ) {
	$font_family = cs_get_option('submenu_item_family');
	if(!empty($font_family['family'])){ ?> font-family: "<?php echo esc_html( $font_family['family'] ); ?>", sans-serif;
<?php }
$variant = calculateFontWeight( $font_family['variant'] );
if(!empty($variant['style'])) : ?> font-style: <?php echo esc_html( $variant['style']); ?> !important;
<?php endif;
if(!empty($variant['weight'])) : ?> font-weight: <?php echo esc_html( $variant['weight']); ?> !important;
<?php endif;
}
if ( cs_get_option('submenu_item_size') ) {
$submenu_item_size = get_number_str(cs_get_option('submenu_item_size')); ?> font-size: <?php echo esc_html( $submenu_item_size ); ?>px;
<?php }

if ( cs_get_option('submenu_line_height') ) {
	$submenu_line_height = get_number_str(cs_get_option('submenu_line_height'));  ?> line-height: <?php echo esc_html( $submenu_line_height ); ?>px;
<?php } ?>
}

<?php if( cs_get_option( 'preloader_image' ) ) :
    $image_src = wp_get_attachment_image_url( cs_get_option( 'preloader_image' ), 'full', false );
?>

@-webkit-keyframes scaleout-image {
    0% {
        -webkit-transform: scale(0.5);
    }
    100% {
        -webkit-transform: scale(1);
        opacity: 0;
    }
}

@keyframes scaleout-image {
    0% {
        transform: scale(0.5);
        -webkit-transform: scale(0.5);
    }
    100% {
        transform: scale(1);
        -webkit-transform: scale(1);
        opacity: 0;
    }
}

.animsition-loading {
    background-color: white;
    z-index: 9999;
    background-image: url(<?php echo esc_url( $image_src ); ?>) !important;
    background-repeat: no-repeat !important;
    background-position: center center !important;
}

.animsition-loading:before {
    display: none;
}

<?php endif; ?>