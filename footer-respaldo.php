<?php
/**
 *
 * Footer
 * @since 1.0.0
 * @version 1.0.0
 *
 */

if ( is_page() || is_home() ) {
	$post_id = get_queried_object_id();
} else {
	$post_id = get_the_ID();
}
// page options
$meta_data           = get_post_meta( $post_id, '_custom_page_options', true );
$meta_data_portfolio = get_post_meta( $post_id, 'awa_portfolio_options', true );
$meta_data_events    = get_post_meta( get_the_ID(), 'awa_events_options', true );
$class_footer        = ! empty( $meta_data['fixed_transparent_footer'] ) || is_404() || ( ! empty( $meta_data_portfolio['fixed_transparent_footer'] ) || ! empty( $meta_data_events['fixed_transparent_footer'] ) ) ? ' fix-bottom' : '';

$enable_footer_copy     = isset( $meta_data['enable_footer_copy_page'] ) ? $meta_data['enable_footer_copy_page'] : cs_get_option( 'enable_footer_copy' );
$enable_footer_socials  = isset( $meta_data['enable_footer_socials'] ) ? $meta_data['enable_footer_socials'] : cs_get_option( 'enable_footer_socials' );
$enable_footer_widgets  = isset( $meta_data['enable_footer_widgets_page'] ) ? $meta_data['enable_footer_widgets_page'] : cs_get_option( 'enable_footer_widgets' );
$enable_footer_white    = isset( $meta_data['enable_footer_white_page'] ) ? $meta_data['enable_footer_white_page'] : cs_get_option( 'enable_footer_white' );
$enable_footer_parallax = isset( $meta_data['enable_parallax_footer_page'] ) ? $meta_data['enable_parallax_footer_page'] : cs_get_option( 'enable_parallax_footer' );
$copyright_align        = isset( $meta_data['awa_copyright_align'] ) ? $meta_data['awa_copyright_align'] : cs_get_option( 'awa_copyright_align' );
$socials_align          = isset( $meta_data['awa_socials_align'] ) ? $meta_data['awa_socials_align'] : cs_get_option( 'awa_socials_align' );


if ( empty( $copyright_align ) ) {
	$copyright_align = 'center';
}

if ( empty( $socials_align ) ) {
	$socials_align = 'center';
}


if ( isset( $meta_data_portfolio['enable_footer_copy_page'] ) ) {
	$enable_footer_copy = $meta_data_portfolio['enable_footer_copy_page'];
} elseif ( isset( $meta_data_events['enable_footer_copy_page'] ) ) {
	$enable_footer_copy = $meta_data_events['enable_footer_copy_page'];
}

if ( isset( $meta_data_portfolio['enable_footer_socials'] ) ) {
	$enable_footer_socials = $meta_data_portfolio['enable_footer_socials'];
} elseif ( isset( $meta_data_events['enable_footer_socials'] ) ) {
	$enable_footer_socials = $meta_data_events['enable_footer_socials'];
}

if ( isset( $meta_data_portfolio['enable_footer_widgets_page'] ) ) {
	$enable_footer_widgets = $meta_data_portfolio['enable_footer_widgets_page'];
} elseif ( isset( $meta_data_events['enable_footer_widgets_page'] ) ) {
	$enable_footer_widgets = $meta_data_events['enable_footer_widgets_page'];
}

if ( function_exists( 'cs_framework_init' ) && ! $enable_footer_copy && ! $enable_footer_widgets && ! $enable_footer_socials ) {
	$class_footer .= ' no-footer';
}

if ( $enable_footer_white ) {
	$class_footer .= ' white-footer';
}

$enable_footer_parallax = apply_filters( 'awa_blog_footer_style', $enable_footer_parallax );
if ( $enable_footer_parallax ) {
	$class_footer .= ' footer-parallax';
}

if ( $enable_footer_copy && $enable_footer_socials ) {
	$copyClass   = 'col-sm-6';
	$socialClass = '';
} else {
	$copyClass   = '';
	$socialClass = ' text-center';
}

if ( is_page() || is_home() ) {
	$post_id = get_queried_object_id();
} else {
	$post_id = get_the_ID();
}


$meta_data           = get_post_meta( $post_id, '_custom_page_options', true );
$meta_data_portfolio = get_post_meta( $post_id, 'awa_portfolio_options', true );
$meta_data_events    = get_post_meta( $post_id, 'awa_events_options', true );

$awa_footer_style = cs_get_option( 'awa_footer_style' ) ? cs_get_option( 'awa_footer_style' ) : 'modern';

if ( isset( $meta_data_portfolio['awa_footer_style'] ) ) {
	$awa_footer_style = $meta_data_portfolio['awa_footer_style'];
} elseif ( isset( $meta_data['awa_footer_style'] ) && $meta_data['awa_footer_style'] ) {
	$awa_footer_style = $meta_data['awa_footer_style'];
}

$container = $awa_footer_style == 'modern' ? 'container no-padd' : 'container-fluid';

$class_footer .= ' ' . $awa_footer_style;
?>

</div>

<?php if ( ! is_404() ) { ?>
    <footer id="footer" class="<?php echo esc_attr( $class_footer ); ?>">

		<?php if ( ! function_exists( 'cs_framework_init' ) ) { ?>
            <div class="container-fluid">
                <div class="copyright text-center">
					<?php
					$footer_text = esc_html__( ' &copy;', 'awa' ) . date( 'Y' );
					echo wp_kses_post( $footer_text . bloginfo( 'name' ) );
					?>
                </div>
            </div>
		<?php } ?>

        <div class="<?php echo esc_attr( $container ); ?>">
			<?php if ( ! function_exists( 'cs_framework_init' ) || ( ! empty( $enable_footer_widgets ) && $enable_footer_widgets == true ) ) {
				$sidebarWidg = wp_get_sidebars_widgets();
				if ( $awa_footer_style == 'modern' && ! empty( $sidebarWidg['footer_sidebar'] ) ) { ?>
                    <div class="widg clearfix">
						<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer_sidebar' ) ); ?>
                    </div>
				<?php } elseif ($awa_footer_style == 'simple' && ! empty( $sidebarWidg['footer_simple_sidebar'] ) ) { ?>
                    <div class="widg clearfix">
						<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer_simple_sidebar' ) ); ?>
                    </div>
				<?php } ?>

			<?php } ?>

			<?php if ( ( ! empty( $enable_footer_copy ) && $enable_footer_copy == true ) || $enable_footer_socials == true ) { ?>

				<?php if ( $enable_footer_socials == true && cs_get_option( 'footer_social' ) ) { ?>
                    <div class="col-xs-12 footer-socials text-<?php echo esc_attr( $socials_align . ' ' . $copyClass . $socialClass ); ?>">
						<?php foreach ( cs_get_option( 'footer_social' ) as $link ) { ?>
                            <a href="<?php echo esc_url( $link['footer_social_link'] ); ?>" target="_blank">
                                <i class="<?php echo esc_attr( $link['footer_social_icon'] ); ?>"></i>
                            </a>
						<?php } ?>
                    </div>
				<?php } ?>

				<?php if ( $enable_footer_copy == true ) { ?>
                    <div class="copyright col-xs-12 text-<?php echo esc_attr( $copyright_align . ' ' . $copyClass ); ?>">
						<?php $footer_text = cs_get_option( 'footer_text' ) ? cs_get_option( 'footer_text' ) : ' ';
						echo wp_kses_post( $footer_text ); ?>
                    </div>
				<?php } ?>

			<?php } ?>
        </div>
    </footer>
<?php } ?>

<?php
$classCopy = cs_get_option( 'enable_copyright' ) && ! cs_get_option( 'text_copyright' ) ? '' : 'copy';
if ( cs_get_option( 'enable_copyright' ) ): ?>
    <div class="awa_copyright_overlay <?php echo esc_attr( $classCopy ); ?>">
        <div class="awa_copyright_overlay-active">
			<?php if ( cs_get_option( 'text_copyright' ) ) : ?>
                <div class="awa_copyright_overlay_text">
					<?php echo wp_kses_post( cs_get_option( 'text_copyright' ) ); ?>
                </div>
			<?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<div class="fullview">
    <div class="fullview__close"></div>
</div>

<?php if ( cs_get_option( 'page_scroll_top' ) == true ) { ?>
    <div class="scroll-top-button">
        <a href="#" id="back-to-top">&uarr;</a>
    </div>
<?php } ?>

<?php wp_footer(); ?>
</body>
</html>