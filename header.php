<?php
/**
 *
 * The Header for Awa theme
 * @since 1.0.0
 * @version 1.0.0
 *
 */

if (is_page() || is_home()) {
    $post_id = get_queried_object_id();
} else {
    $post_id = get_the_ID();
}

$meta_data = get_post_meta($post_id, '_custom_page_options', true);
$meta_data_portfolio = get_post_meta($post_id, 'awa_portfolio_options', true);
$menu_main_style = is_404() ? 'only_logo' : cs_get_option('menu_style');
$menu_main_style = !empty($menu_main_style) ? $menu_main_style : 'left';
$menu_full_w_class = '';
$enable_search = cs_get_option('search_on');

if (isset($meta_data['full_width_menu']) && $meta_data['full_width_menu']) {
    $menu_full_w_class = 'full-width-menu';
}
if (isset($meta_data['change_menu']) && $meta_data['change_menu'] && isset($meta_data['menu_style'])) {
    $menu_main_style = $meta_data['menu_style'];
}

if (isset($meta_data_portfolio['change_menu']) && $meta_data_portfolio['change_menu'] && isset($meta_data_portfolio['menu_style'])) {
	$menu_main_style = $meta_data_portfolio['menu_style'];
}

global $bodyClass;
$mobButtonClass = '';

if ($menu_main_style && $menu_main_style == 'right') {
    $menu_class = 'right-menu';
} elseif ($menu_main_style && $menu_main_style == 'compact') {
    $menu_class = 'right-menu compact';
} elseif ($menu_main_style && $menu_main_style == 'left') {
    $menu_class = 'right-menu left';
} elseif ($menu_main_style && $menu_main_style == 'aside') {
    $menu_class = 'right-menu aside-menu aside-fix';
} elseif ($menu_main_style && $menu_main_style == 'static_aside') {
    $menu_class = 'right-menu aside-menu static';
    $bodyClass = 'static-menu';
} elseif ($menu_main_style && $menu_main_style == 'full') {
	$mobButtonClass = 'mob-but-full';
    $menu_class = 'right-menu compact full';
}else {
    $menu_class = 'top-menu';
}
$vertical_logo = cs_get_option('vertical_logo');
$aside_open = cs_get_option('aside_open');


if (isset($meta_data['change_menu']) && $meta_data['change_menu'] && isset($meta_data['vertical_logo'])) {
    $vertical_logo = $meta_data['vertical_logo'];
}

if (isset($meta_data['change_menu']) && $meta_data['change_menu'] && isset($meta_data['aside_open'])) {
    $aside_open = $meta_data['aside_open'];
}


$aside_open = isset($aside_open) && $aside_open && $menu_main_style == 'aside' ? 'active-menu' : '';
$aside_open .= isset($vertical_logo) && $vertical_logo && $menu_main_style == 'aside' ? ' vertical' : '';
$menu_class = !function_exists('cs_framework_init') ? 'right-menu left' : $menu_class;
$menu_class = !function_exists('cs_framework_init') && is_404() ? 'top-menu' : $menu_class;
$class_main_wrapper = '';
$menu_class = apply_filters('awa_menu_style', $menu_class);

$preloader_text = cs_get_option('preloader_text') ? cs_get_option('preloader_text') : 'w';
$preloader_images = cs_get_option('preloader_images') ? cs_get_option('preloader_images') : '';


// page options
$enable_footer_parallax = isset($meta_data['enable_parallax_footer_page']) ? $meta_data['enable_parallax_footer_page'] : cs_get_option('enable_parallax_footer');
$enable_footer_parallax = apply_filters('awa_blog_footer_style', $enable_footer_parallax);
$unitClass = !function_exists('cs_framework_init') ? ' unit ' : '';
$blog_type = cs_get_option('blog_single_type') ? cs_get_option('blog_single_type') : 'modern';

if ($enable_footer_parallax) {
    $class_main_wrapper .= ' footer-parallax';
}


$class_desc_padd = '';
$class_mob_padd = '';

if (!empty($meta_data['padding_desktop'])) {
    $class_desc_padd = ' padding-desc ';
}
if (!empty($meta_data['padding_mobile'])) {
    $class_mob_padd = ' padding-mob ';
}

$center_menu = cs_get_option('center_menu') ? 'center-menu' : '';
$center_menu = isset($meta_data['center_menu']) && !empty($meta_data['center_menu']) ? 'center-menu' : $center_menu;
$center_menu = isset($meta_data_portfolio['center_menu']) && !empty($meta_data_portfolio['center_menu']) ? 'center-menu' : $center_menu;

$center_menu = $menu_main_style == 'left' ? $center_menu : '';

$enable_sound_mob = cs_get_option('enable_sound_mob');
$enable_sound_mob = isset($enable_sound_mob) ? $enable_sound_mob : true;
$mobile      = cs_get_option( 'mobile_menu' );
$bodyClass   .= isset( $mobile ) && $mobile ? ' mob-main-menu' : '';
$bodyClass   .= cs_get_option( 'enable_sound' ) ? ' enable_sound' : '';
$bodyClass .= $enable_sound_mob ? ' enable_sound_mob' : '';
$mobileWidth = isset($mobile) && $mobile ? '1024' : '992';  ?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,800" rel="stylesheet">


</head>
<body <?php body_class(); ?>>
<!-- MAIN_WRAPPER -->
<?php
$class_animsition = 'animsition' . $unitClass;
if (cs_get_option('awa_disable_preloader') || cs_get_option('preloader_type') == 'text' || cs_get_option('preloader_type') == 'spinner') {
    $class_animsition = '';
}

if (!cs_get_option('awa_disable_preloader') && cs_get_option('preloader_type') == 'text') { ?>
  <div class="preloader-svg">
    <svg id="loaderSvg" class="loader__svg" xmlns="http://www.w3.org/2000/svg" height="300" viewBox="0 0 1024 300"
         style="opacity: 1;">
        <?php if (!empty($preloader_images)){ ?>
      <defs>
          <?php
          $preloader_images = explode(",", $preloader_images);
          $count = 1;
          foreach ($preloader_images as $image) { ?>
            <pattern x="0" y="0" width="1024" height="686" patternUnits="userSpaceOnUse"
                     id="pattern<?php echo esc_attr($count); ?>" viewBox="0 0 1024 300">
              <image xmlns:xlink="http://www.w3.org/1999/xlink"
                     xlink:href="<?php echo esc_url(wp_get_attachment_image_url($image, 'large')); ?>"
                     preserveAspectRatio="none" x="0" y="-200" width="1024" height="686"></image>
            </pattern>
              <?php $count++;
          } ?>
      </defs>
      <text x="50%" y="20%" id="letter" dy="150" style="text-anchor: middle; font-size: 180px; font-weight: bold;"
            fill="url('#pattern1')">
          <?php }else{ ?>
        <text x="50%" y="20%" id="letter" dy="150" style="text-anchor: middle; font-size: 180px; font-weight: bold;"
              fill="#222222">
            <?php } ?>
            <?php echo esc_html($preloader_text); ?>
        </text>
    </svg>
  </div>
<?php }

if(!cs_get_option('awa_disable_preloader') && cs_get_option('preloader_type') == 'spinner'){ ?>
    <div class="spinner-preloader-wrap">
        <div class="cssload-container">
            <div class="cssload-whirlpool"></div>
        </div>
    </div>
<?php }?>

<div
    class="main-wrapper <?php echo esc_attr($class_animsition . $class_main_wrapper) . ' ' . esc_attr($class_desc_padd) . esc_attr($class_mob_padd); ?>" data-sound="<?php echo esc_url(get_template_directory_uri() . '/assets/audio/'); ?>" data-top="<?php echo esc_attr($mobileWidth); ?>">
    <?php
    $fixed_menu_class = (cs_get_option('fixed_menu') || is_404()) && ($bodyClass != 'static-menu') ? ' enable_fixed' : '';

    if (($bodyClass != 'static-menu') && $menu_main_style != 'aside' && $menu_main_style != 'static_aside') {
        if (isset($meta_data['style_header']) && $meta_data['style_header'] === 'transparent') {
            $fixed_menu_class .= ' header_trans-fixed';
        } elseif (isset($meta_data_portfolio['style_header']) && $meta_data_portfolio['style_header'] === 'transparent') {
            $fixed_menu_class .= ' header_trans-fixed';
        } elseif (is_404() || (cs_get_option('fixed_transparent_menu_blog') && is_single() && get_post_type() == 'post')) {
            $fixed_menu_class .= ' header_trans-fixed';
        } elseif (cs_get_option('fixed_menu') && cs_get_option('transparent_menu') && ((isset($meta_data['style_header']) && $meta_data['style_header'] === 'empty') || (isset($meta_data_portfolio['style_header']) && $meta_data_portfolio['style_header'] === 'empty'))) {
            $fixed_menu_class .= ' header_trans-fixed';
        } elseif ((isset($meta_data['style_header']) && $meta_data['style_header'] === 'fixed') || (isset($meta_data_portfolio['style_header']) && $meta_data_portfolio['style_header'] === 'fixed')) {
            $fixed_menu_class .= ' fixed-header';
        } elseif ((isset($meta_data['style_header']) && $meta_data['style_header'] === 'none') || (isset($meta_data_portfolio['style_header']) && $meta_data_portfolio['style_header'] === 'none')) {
            $fixed_menu_class .= ' header_trans-fixed none';
        } else {
            $fixed_menu_class .= '';
        }
    }

    $menu_light_text = ((isset($meta_data['menu_light_text']) && $meta_data['menu_light_text'] && $meta_data['style_header'] == 'transparent') || (isset($meta_data_portfolio['menu_light_text']) && $meta_data_portfolio['menu_light_text'] ))? 'menu_light_text' : '';

    $fixed_menu_class = !function_exists('cs_framework_init') && is_404() ? ' fixed-header' : $fixed_menu_class;
    $fixed_menu_class = is_single() && get_post_type() == 'post' && $blog_type == 'modern' && !has_post_thumbnail() && !(isset($post_meta[0]['post_preview_style']) && ($post_meta[0]['post_preview_style'] != 'audio' || $post_meta[0]['post_preview_style'] != 'slider' || $post_meta[0]['post_preview_style'] != 'video')) ? '' : $fixed_menu_class;
    $fixed_menu_class = apply_filters('awa_blog_menu_style', $fixed_menu_class);

    if($menu_class == 'right-menu aside-fix aside-menu' || 'right-menu aside-fix aside-menu vertical'){ ?>
        <div class="header_top_bg <?php echo esc_attr($fixed_menu_class . ' ' . $menu_light_text . ' ' . $menu_full_w_class . ' ' . $center_menu) ?>" style="padding-bottom: 0;">
    <?php }else{ ?>
        <div class="header_top_bg <?php echo esc_attr($fixed_menu_class . ' ' . $menu_light_text . ' ' . $menu_full_w_class . ' ' . $center_menu) ?>">
    <?php }?>


    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">

          <!-- HEADER -->
          <header class="<?php echo esc_attr($menu_class . ' ' . $aside_open); ?>">
            <!-- LOGO -->
              <?php awa_site_logo(); ?>
            <!-- /LOGO -->
              <?php if($menu_main_style != 'only_logo'){ ?>
	              <?php if (!($menu_main_style && ($menu_main_style == 'compact' || $menu_main_style == 'full'))) { ?>
                      <!-- MOB MENU ICON -->
                      <a href="#" class="mob-nav">
                          <div class="hamburger">
                              <span class="line"></span>
                              <span class="line"></span>
                              <span class="line"></span>
                          </div>
                      </a>
                      <!-- /MOB MENU ICON -->
	              <?php } ?>


                  <!-- ASIDE MENU ICON -->
                  <a href="#" class="aside-nav">
                      <span class="aside-nav-line line-1"></span>
                      <span class="aside-nav-line line-2"></span>
                      <span class="aside-nav-line line-3"></span>
                  </a>
                  <!-- /ASIDE MOB MENU ICON -->

                  <!-- NAVIGATION -->
                  <nav id="topmenu" class="topmenu <?php echo esc_attr($aside_open); ?>">
                      <a href="#" class="mob-nav-close">
                          <span>close</span>
                          <div class="hamburger">
                              <span class="line"></span>
                              <span class="line"></span>
                          </div>
                      </a>
		              <?php if ($menu_main_style && $menu_main_style == 'full'){ ?>
                      <div class="full-menu-wrap">
			              <?php } ?>
			              <?php awa_custom_menu(); ?>
			              <?php if ($menu_main_style != 'full' && function_exists('cs_framework_init')) { ?>
                              <span class="f-right">
                                <?php if (cs_get_option('header_social') && $menu_main_style != 'left') { ?>
                                  <div class="awa-top-social">
                                    <span class="social-icon fa fa-share-alt"></span>
                                    <ul class="social">
                                      <?php foreach (cs_get_option('header_social') as $link) { ?>
                                          <li><a href="<?php echo esc_url($link['header_social_link']); ?>" target="_blank"><i
                                                          class="<?php echo esc_attr($link['header_social_icon']); ?>"></i></a>
                                        </li>
                                      <?php } ?>
                                    </ul>
                                  </div>
                              <?php }

                      if (function_exists('WC')) {
	                      if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) && (cs_get_option('shop_cart_on') || !function_exists('cs_framework_init'))) {

		                      $count = WC()->cart->cart_contents_count; ?>
                              <div class="mini-cart-wrapper">
                                <a class="awa-shop-icon fa fa-shopping-cart" href="<?php echo wc_get_cart_url(); ?>"
                                   title="<?php esc_attr_e('view your shopping cart', 'awa'); ?>">
                                  <?php if ($count > 0) { ?>
                                      <div class="cart-contents">
                                      <span class="cart-contents-count"><?php echo esc_html($count); ?></span>
                                    </div>
                                  <?php } ?>
                                </a>
			                      <?php echo awa_mini_cart(); ?>
                              </div>
	                      <?php }
                      } ?>

					              <?php if ($menu_main_style == 'left' && $enable_search) { ?>
                                      <span class="search-icon-wrapper">
                              <i class="fa fa-search open-search"></i>
							              <?php do_action('awa_after_footer'); ?>
                            </span>
					              <?php } ?>

					              <?php if ($menu_main_style == 'static_aside') {
						              $footer_text = cs_get_option('static_text') ? cs_get_option('static_text') : ' '; ?>
                                      <div class="copy">
                      <?php echo wp_kses_post($footer_text); ?>
                  </div>
					              <?php } ?>
</span>
			              <?php } ?>

			              <?php if ($menu_main_style && $menu_main_style == 'full') {
			              $additional_text = cs_get_option('full_text'); ?>
                          <div class="info-wrap">
				              <?php if(!empty($additional_text)){ ?>
                                  <div class="additional">
						              <?php echo wpautop($additional_text); ?>
                                  </div>
				              <?php } ?>
                              <div class="search">
					              <div><?php esc_html_e('search', 'awa'); ?></div>
					              <?php do_action('awa_after_footer'); ?>
                              </div>
                          </div>


			              <?php $footer_textFull = cs_get_option('static_text') ? cs_get_option('static_text') : ' '; ?>
                          <div class="copy">
				              <?php echo wp_kses_post($footer_textFull); ?>
                          </div>
                      </div>
	              <?php } ?>
                  </nav>
                  <!-- NAVIGATION -->

	              <?php if ($menu_main_style && ($menu_main_style == 'compact' || $menu_main_style == 'full')) { ?>
                      <!-- MOB MENU ICON -->
                      <a href="#" class="mob-nav <?php echo esc_attr($mobButtonClass);?>">
                          <span><?php esc_html_e('close', 'awa'); ?></span>
                          <div class="hamburger">
                              <span class="line"></span>
                              <span class="line"></span>
                              <span class="line"></span>
                          </div>
                      </a>
                      <!-- /MOB MENU ICON -->
	              <?php } ?>

              <?php } ?>

          </header>
        </div>
      </div>

    </div>
  </div>